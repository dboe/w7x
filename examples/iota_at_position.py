#!/usr/bin/env
"""
Calculate the iota at a point in space.

TODO(@dboe): check the original intention of the example.

Author: andrea.nerlo@ipp.mpg.de
"""

import tfields
import w7x
import w7x.simulation.flt


def test_iota_at_position_example():
    """retreive iota value"""
    coil_set = w7x.config.CoilSets.AsBuilt()
    points = tfields.Points3D([[6.2, 0.0, 0.0]])

    parameters = dict(points=points, step=1e-3, inverse_field=False)
    magnetic_characteristics = (
        w7x.simulation.flt.FieldLineTracer().magnetic_characteristics(
            coil_set, **parameters
        )
    )

    print("Iota: ", magnetic_characteristics[0].iota)


if __name__ == "__main__":
    test_iota_at_position_example()
