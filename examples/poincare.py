# pylint:disable=abstract-class-instantiated
"""
tracing the line of sight to one phi in the machine

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import logging
import numpy as np
import rna.plotting
import tfields
import w7x
import w7x.simulation.flt
import w7x.simulation.components

logging.basicConfig(level=logging.INFO)


def test_poincare_example():
    """poincare plot creation"""
    phi = 171.2 / 180 * np.pi
    assembly = w7x.model.Assembly(
        components=[
            w7x.config.AssemblyGroups.Vessel(),
            w7x.config.AssemblyGroups.Divertor(),
        ]
    )

    flt = w7x.simulation.flt.FieldLineTracer()
    components = w7x.simulation.components.Components()
    with w7x.distribute(True):
        graph = w7x.State.merged(
            components.mesh_slice(assembly, phi=phi),
            # pylint:disable=no-value-for-parameter
            flt.trace_poincare(
                phi=phi,
                seeds=tfields.Points3D([[5.8, 0.0, 0.0], [6.0, 0.0, 0.0]]),
                n_points=100,
            ),
        )
        # graph.visualize(filename="poincare_graph.pdf")
        state = w7x.compute(graph)  # , cache="poincare_state.dill")

    # plotting
    rna.plotting.set_style()
    poincare_figure = w7x.plotting.Poincare()
    poincare_figure.gca().grid(color="lightgrey")
    poincare_figure.plot(state, phi=phi)

    rna.plotting.save("~/tmp/poincare", extension=["png", "pgf", "pdf"])
    rna.plotting.show()


if __name__ == "__main__":
    test_poincare_example()
