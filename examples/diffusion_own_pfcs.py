"""
Load your own plasma facing commponents additional to predefined components and run field line
diffusion to get the load/hit points
"""
import tfields
import w7x
import w7x.simulation.flt


def test_diffusion_own_pfcs_example():
    example_stp = tfields.Mesh3D.load(w7x.config.components.test.example_stp)
    example_stp /= 1e3  # convert to meters
    # import rna
    # example_stp.plot(dim=3)
    # rna.plotting.show()
    own_component = w7x.model.Component(
        name="wolfram tile 232044",
        # info="Info if wished.",
        mesh=example_stp,
    )
    assembly = w7x.model.Assembly()
    assembly.add(w7x.config.AssemblyGroups.Divertor())
    assembly.add(own_component)

    flt = w7x.simulation.flt.FieldLineTracer()
    # with w7x.distribute(True):
    #     graph = flt.diffusion(assembly, start_points=10)
    #     graph.visualize(filename="full_diff_graph.pdf")
    #     state = graph.compute()
    state = flt.diffusion(assembly, start_points=10)

    print(state.psi)


if __name__ == "__main__":
    test_diffusion_own_pfcs_example()
