#!/usr/bin/env
"""
tracing the line of sight to one phi in the machine

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import w7x


def test_langumir_connection_example():
    # define w7x run
    run = w7x.flt.Run()

    # define initial points to trace
    lm_probes = (
        w7x.Points3D(
            [
                [865.719, -5452.778, -1023.417],
                [861.917, -5428.400, -1019.385],
                [858.116, -5404.021, -1015.353],
                [854.315, -5379.643, -1011.322],
                [850.514, -5355.265, -1007.290],
                [839.110, -5282.130, -995.197],
                [835.309, -5257.752, -991.166],
                [831.508, -5233.374, -987.135],
                [827.706, -5208.995, -983.104],
                [823.905, -5184.617, -979.074],
                [922.352, -5459.780, -1022.304],
                [918.333, -5435.426, -1018.337],
                [914.314, -5411.072, -1014.370],
                [910.295, -5386.719, -1010.403],
                [906.276, -5362.365, -1006.436],
                [894.220, -5289.303, -994.535],
                [890.201, -5264.949, -990.569],
                [886.182, -5240.595, -986.602],
                [882.164, -5216.241, -982.636],
                [878.145, -5191.887, -978.669],
            ]
        )
        / 1e3
    )

    # trace both directions
    cl = run.connection_length(lm_probes)
    run.inverseField = True
    cl_i = run.connection_length(lm_probes)

    # report
    comp_db_server = w7x.get_server(w7x.Server.addr_components_db)
    print("Trace terminating on component: ", cl.parts_mm_id())
    print(
        "First part of descriptions from componentsdb: ",
        [
            comp_db_server.service.getComponentInfo(mm_id)[0].comment.split(".")[0]
            for mm_id in cl.parts_mm_id()
        ],
    )
    print("Precise terminating points: ", cl.hits)

    print("Trace terminating on component: ", cl_i.parts_mm_id())
    print(
        "First part of descriptions from componentsdb: ",
        [
            comp_db_server.service.getComponentInfo(mm_id)[0].comment.split(".")[0]
            for mm_id in cl_i.parts_mm_id()
        ],
    )
    print("Precise terminating points: ", cl_i.hits)


if __name__ == "__main__":
    test_langumir_connection_example()
