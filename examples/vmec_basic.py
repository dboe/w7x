"""
Example on how to run VMEC.

How to run me?

$ module load anaconda # if you are logged in the HPC
$ python examples/vmec.py
"""

import logging

import w7x
from w7x.simulation.vmec import Vmec


def test_vmec_basic_example():
    logging.basicConfig(level=logging.INFO)

    # Define a default state
    state = w7x.State(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
    )

    # Compute a free boundary equilibrium
    vmec = Vmec()
    state = vmec.free_boundary(state)

    # Save the state for later working with it
    # state.save("vmec_ideal_vacuum_case.dill")

    # Inspect the computed equilibrium
    print(f"Plasma volume: {state.equilibrium.plasma_volume}")


if __name__ == "__main__":
    test_vmec_basic_example()
