"""
Toy example to investigate dask integration.
"""
import w7x
from time import sleep, time


@w7x.node
def compute_equilibrium(state):
    """
    an equilibrium code compute method.
    """
    equi = w7x.config.Equilibria.InitialGuess()
    equi.kinetic_energy = 10.0
    sleep(1)
    return equi


@w7x.node
def compute_transport(state):
    """
    A transport code compute method.
    """
    plasma = w7x.config.Plasma.Vacuum()
    plasma.diffusion_coeff = 4.2
    sleep(1)
    return plasma


@w7x.node
def compute_difference(a, b):
    return a.equilibrium.kinetic_energy - b.equilibrium.kinetic_energy


def test_distributed_computing_example():
    t0 = time()
    # change the boolean to see the time difference
    with w7x.distribute(True):
        #  Build the computational graph
        state = w7x.State.merged(
            w7x.config.Plasma.Vacuum(), w7x.model.Equilibrium(kinetic_energy=52)
        )

        equi = compute_equilibrium(state)
        plasma = compute_transport(state)

        #  Merge the state
        state = w7x.State.merged(state, plasma)

        #  Perform computation on both state attributes
        diff = compute_difference(state, w7x.State.merged(equi))

        if w7x.distribute.enabled():
            res = w7x.compute(diff)
            # To export this as a pdf, uncomment the following code ()
            # diff.visualize(
            #     filename=w7x.config.general.tmp_dir + "/distributed_computing_graph.pdf"
            # )
            print("Distributed computing is active")
        else:
            res = diff
            print("Distributed computing is not active")
    print("The result is: ", res)
    print("Runtime      : ", time() - t0)
    assert res == 42


if __name__ == "__main__":
    test_distributed_computing_example()
