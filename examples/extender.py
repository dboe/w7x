"""
Example on how to run EXTENDER.

How to run me?

$ python examples/extender.py -h
"""

import argparse
import os

import tfields
import numpy as np
import rna.parsing
import rna.plotting
import w7x
from w7x.simulation.vmec import Wout
from w7x.simulation.extender import Extender
from w7x.simulation.flt import FieldLineTracer
from w7x.plotting.extender import FieldFigure


def test_extender_example():
    """
    Build a graph and compute it.
    """
    parser = argparse.ArgumentParser(parents=[rna.parsing.LogParser()])
    parser.add_argument("-n", type=int, default=4, help="number of cores")
    parser.add_argument(
        "--distribute", "-d", action="store_true", help="distribute the work load"
    )
    parser.add_argument(
        "--realistic", "-r", action="store_true", help="Compute a high res field"
    )
    parser.add_argument("--load", action="store_true", help="Load the state from file")
    parser.add_argument(
        "--export",
        action="store_true",
        help="Export the field to .datc file. -> fast communication with the web service.",
    )
    parser.add_argument(
        "--poincare", action="store_true", help="Plot the poincare cross section"
    )
    parser.add_argument(
        "--vmec_id",
        type=str,
        default=w7x.config.vmec.test.example_vmec_id,
        help="vmec_id of the run",
    )
    args = parser.parse_args()

    wout = Wout(file_id=args.vmec_id)
    state = wout.to_state()

    extender = Extender()

    if args.realistic:
        path = f"extended_field_of_vmec_id_{args.vmec_id}.dill"
        if args.load and os.path.exists(path):
            state = w7x.State.load(path)
        else:
            # The full field takes much longer
            state = extender.field(
                state,
            )

            # Save the state for later working with it
            state.save(path)

    else:
        path = f"extended_field_of_vmec_id_{args.vmec_id}_short.dill"
        # You can change the resolution of the grid by passing the points explicitly:
        # Here we show with a dummy grid which has uselessly small resolution
        if args.load and os.path.exists(path):
            state = w7x.State.load(path)
        else:
            state = extender.field(
                state,
                points=tfields.TensorGrid.empty(
                    (4.05, 6.75, 10j),
                    (
                        0,
                        2 * np.pi / 10,
                        20j,
                    ),  # np.pi / 10 -> 5 field periods, 2 fold symmetry
                    (0, 0, 1j),
                    coord_sys=tfields.bases.CYLINDER,
                    iter_order=[1, 0, 2],
                ),
            )

    if args.export:
        # TODO-1(@dboe) make this a function
        path = path.rstrip(".dill") + ".datc"
        # state.equilibrium.export("field", path)
        state.equilibrium.field.save(path)
        file_ = w7x.model.MGrid(data=state.equilibrium.field, path=path)
        state.equilibrium.field = rna.pattern.link.Link(
            ref=file_, fget=w7x.model.MGrid.to_numpy
        )
        state.resources.add(file_)

    ##########################################
    # display the field with a poincare plot #
    ##########################################
    if args.poincare:
        flt = FieldLineTracer()
        phi = 0
        state = flt.trace_poincare(
            state,
            phi=phi,
            seeds=tfields.Points3D([[5.8, 0.0, 0.0], [6.0, 0.0, 0.0]]),
            n_points=100,
        )
        rna.plotting.set_style()
        poincare_figure = w7x.plotting.Poincare()
        poincare_figure.gca().grid(color="lightgrey")
        poincare_figure.plot(state, phi=phi)
        rna.plotting.show()

    ####################################
    # Inspect the computed equilibrium #
    ####################################
    field = state.equilibrium.field

    # plot the field in the z=0 plane
    plot_field = field
    # plot_field = plot_field[plot_field[:, 1] == 0]  # select phi = 0
    # plot_field = plot_field[plot_field[:, 2] == 0]  # select z = 0
    plot_field = plot_field[
        w7x.lib.tfields.where_phi_between(plot_field, 0, 2 * np.pi / 5)
    ]
    plot_field = plot_field[plot_field[:, 2] == np.abs(plot_field[:, 2]).min()]
    plot_field.transform_field(tfields.bases.NATURAL_CARTESIAN, field_index=0)
    plot_field.transform(tfields.bases.CARTESIAN)
    fig = FieldFigure(plot_field)
    fig.plot()
    fig.show()


if __name__ == "__main__":
    test_extender_example()
