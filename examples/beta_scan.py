"""
Beta scan with an equilibrium code.

Mainly used to provide the grid values for beta fitting used in
the mock VMEC backend.

TODO-1(@amerlo): consider to remove me, or to better describe it.
"""

import json
import numpy as np
import pytest
import w7x
from w7x.lib.profiles import TwoPower, PowerSeries
from w7x.simulation.vmec import Vmec


def test_beta_scan_example():
    # skip test if hyperopt is not installed
    pytest.importorskip("torch")

    EJM_252_I1 = 13067.0
    EJM_252_IA = -699.0

    # Beta scan for EJM+252
    # i1 is the current ratio for I1
    # p0 is the pressure on axis for a two power profile
    # phi_edge is the total magnetic flux at the edge
    i1 = [0.9, 0.95, 1.0, 1.05, 1.1]
    p0 = [0.0, 1e3, 1e4, 1e5, 2e5]
    phi_edge = [-2.6, -1.6]

    shape = (len(i1), len(p0), len(phi_edge))

    #  Placeholders for the data
    beta = np.zeros(shape)
    # b_axis = np.zeros(shape)
    vol = np.zeros(shape)
    vmec_id = np.empty(shape, dtype="<U41")

    #  Logging information
    # print("{:50s} {:6s} {:6s} {:6s}".format("vmec-id", "beta", "b_axis", "vol"))
    print("{:50s} {:6s} {:6s}".format("vmec-id", "beta", "vol"))

    for i, j, k in [
        (i, j, k)
        for i in range(len(i1))
        for j in range(len(p0))
        for k in range(len(phi_edge))
    ]:
        cur = i1[i]
        pre = p0[j]
        phi = phi_edge[k]

        currents = [cur * c for c in [EJM_252_I1] * 5 + [EJM_252_IA] * 2]
        state = w7x.State(
            w7x.config.CoilSets.Ideal.from_currents(*currents, unit="A"),
            w7x.config.Plasma.Vacuum(
                pressure_profile=TwoPower(coefficients=[pre, 1, 2], domain=[0, 1])
            ),
            w7x.config.Equilibria.InitialGuess(phi=PowerSeries(coefficients=[0, phi])),
        )
        vmec = Vmec(strategy="nn")
        # Use low force tolerance to quickly converge (only for real vmec backends, i.e. not the approximating nn)
        # state = vmec.free_boundary(state, force_tolerance_levels=[1e-3, 1e-5, 1e-9, 1e-10])
        state = vmec.free_boundary(state)

        beta[i, j, k] = state.equilibrium.beta
        # b_axis[i, j, k] = state.equilibrium.b_axis  # TODO: add bmnc from network to netcdf or use local/web_service
        vol[i, j, k] = state.equilibrium.plasma_volume
        #  TODO-0(@amerlo): "*.nc" load has to be fixed first in Mgrid.to_numpy
        #  See TODO in Wout.to_state
        file_id = "FIX_ME"  # state.resources.items[-1].file_id
        vmec_id[i, j, k] = file_id

        print(
            f"{file_id:50s} {state.equilibrium.beta:5.4f} {state.equilibrium.plasma_volume:5.4f}"
        )  # TODO: add {state.equilibrium.b_axis:5.4f}

    data = {
        "x": [i * EJM_252_I1 for i in i1],
        "y": p0,
        "z": phi_edge,
        "beta": beta.tolist(),
        # "b_axis": b_axis.tolist(),  # TODO: add bmnc from network to netcdf
        "vol": vol.tolist(),
        "vmec_id": vmec_id.tolist(),
    }

    with open("beta_scan.json", "w") as f:
        json.dump(data, f)


if __name__ == "__main__":
    test_beta_scan_example()
