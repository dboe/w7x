#!/usr/bin/env
"""
Calculate the connection length of one point in both directions

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import tfields
import w7x
import w7x.simulation.flt


def test_connection_length():
    """retreive connection length"""
    assembly = w7x.model.Assembly()
    assembly.add(w7x.config.AssemblyGroups.Vessel())
    assembly.add(w7x.config.AssemblyGroups.Shield())
    coil_set = w7x.config.CoilSets.AsBuilt()
    start_points = tfields.Points3D([[5.6, 0.0, 0.0], [6.0, 0.0, 0.0]])

    parameters = dict(start_points=start_points, connection_limit=3000)
    state = w7x.simulation.flt.FieldLineTracer().connection_length(
        coil_set, assembly, **parameters
    )
    # Explicit use with state:
    # state = w7x.State(assembly, coil_set)
    # graph = w7x.simulation.flt.FieldLineTracer().trace_connection_length(state,
    #     start_points=start_points
    # )
    # state = graph.compute()
    # con_len = state.traces.connection_lengths

    print("Connection points:", state.pwi.connection_lengths())
    print(
        "Connection lengths between input point and connection points:",
        state.pwi.connection_lengths().fields[0],
    )


if __name__ == "__main__":
    test_connection_length()
