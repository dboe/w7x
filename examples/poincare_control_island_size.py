#!/usr/bin/env
"""
tracing the line of sight to one phi in the machine.
Using standard case magnetic field with sweep/control coils to demonstrate
island width control

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import w7x
import tfields
import numpy as np
import matplotlib.pyplot as plt


def test_poincare_control_island_size_example():
    """poincare plot creation"""
    phi = 171.2 / 180 * np.pi
    i_nominal = 15000 * 108
    i_nominal_sc = float(2500 * 8)
    magnetic_config = w7x.flt.MagneticConfig.from_currents(
        *([i_nominal] * 5 + [0, 0] + [-i_nominal_sc, i_nominal_sc]),  # larger islands
        unit="Aw"
    )
    run = w7x.flt.Run(
        magnetic_config=magnetic_config,
        machine=w7x.flt.Machine.from_mm_ids("divertor", "baffle", "vessel"),
    )

    # plotting
    tfields.plotting.set_style()
    axis = plt.gca()
    axis.grid(color="lightgrey")
    run.plot_poincare(phi, axis=axis)  # auto-seeds
    I_s = i_nominal_sc / i_nominal
    tfields.plotting.save(
        "~/tmp/poincare_control_island_size_-I_s1,I_s2_{I_s:.5f}".format(**locals()),
        "png",
        "pgf",
        "pdf",
    )
    plt.show()


if __name__ == "__main__":
    test_poincare_control_island_size_example()
