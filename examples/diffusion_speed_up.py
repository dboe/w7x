"""
diffuse a extended vmec run and save the result
This example assumes, you have a file at <path> that describes the magnetic
field.
Also it assumes symmetry in components and Magnetic field sinces it maps all
 points to one submodule.
It is commonly having the extension .dat
"""
import logging
from sympy.abc import z

import rna
import tfields
import w7x
from w7x.simulation.flt import FieldLineTracer
import w7x.simulation.components

logging.basicConfig(level=logging.DEBUG)


def test_diffusion_speed_up_example():
    # we instantiate a FLT code and show examples of setting the magnetic field.
    flt = FieldLineTracer()
    components = w7x.simulation.components.Components()

    plasma = w7x.model.PlasmaParameters(diffusion_coeff=9.0, velocity=2e6)

    # Magnetic Config
    n_start = 12500
    kjm_252 = [13230, 12859, 12251, 11642, 11272, 0, 0] + [0.0] * (2 + 5)
    coil_set = w7x.config.CoilSets.Ideal.from_currents(*kjm_252, unit="A")

    # Run a vacuum configuration (manually cached)
    path = rna.path.resolve(w7x.config.general.tmp_dir, "fast_diffusion.dill")
    if rna.path.exists(path):
        state = w7x.State.load(path)
    else:
        state = flt.diffusion(plasma, coil_set, start_points=n_start)
        state.save(path)

    hits = state.pwi.hit_points(
        *w7x.config.AssemblyGroups.Divertor().get_components(flat=True)
    )
    # map the hits to segment 1 upper divertor
    w7x.lib.tfields.to_segment_one(hits, mirror_z=True)

    divertor_comp = w7x.config.AssemblyGroups.Divertor().components[0]
    state = components.set_meshes(state, component=divertor_comp)

    divertor_mesh_with_tree_path = rna.path.resolve(
        w7x.config.general.tmp_dir, "upper_divertor_0_mesh_with_tree.npz"
    )
    if rna.path.exists(path):
        divertor_mesh = tfields.Mesh3D.load(divertor_mesh_with_tree_path)
    else:
        divertor_mesh = state.assembly.components[0].components[0].mesh
        # cut for the upper divertor
        divertor_mesh = divertor_mesh.cut(z >= 0)
        # build the tree if not in cache
        divertor_mesh.tree
        divertor_mesh.save(divertor_mesh_with_tree_path)

    divertor_mesh.in_faces(
        hits, delta=None
    )  # delta=None means that we want the closest face to the point ot be accepted


if __name__ == "__main__":
    test_diffusion_speed_up_example()
