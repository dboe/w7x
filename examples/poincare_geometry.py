#!/usr/bin/env
"""
tracing the line of sight to one phi in the machine

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import w7x
import tfields
import numpy as np
import matplotlib.pyplot as plt


def test_poincare_geometry_example():
    """poincare plot creation"""
    phi = 171.2 / 180 * np.pi
    run = w7x.flt.Run(machine=w7x.flt.Machine.from_mm_ids("vessel"))

    # plotting
    tfields.plotting.set_style()
    axis = plt.gca()
    axis.grid(color="lightgrey")
    run.machine.plot_poincare(phi, axis=axis)
    plt.show()
    # tfields.plotting.save("~/tmp/poincare", 'png', 'pgf', 'pdf')


if __name__ == "__main__":
    test_poincare_geometry_example()
