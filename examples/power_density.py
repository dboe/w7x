"""
Example of the evaluation of the power density (n/A) of a field line diffusion run.

Run this file from
"""
import logging
import rna
import rna.plotting
import w7x
import w7x.simulation.flt
import w7x.simulation.components

logging.basicConfig(level=logging.DEBUG)


def test_power_density_example():
    state_path = w7x.config.flt.test.diffused_state_dill
    # This code allows you to generate the file this takes some time so we
    # skip this here and directly load the state from this file.
    # with rna.log.timeit("Calculation full diffusion runtime:"):
    #     # report of runtimes:
    #     #   standard case: 108 s
    #     n_start = 12500
    #
    #     flt = w7x.simulation.flt.FieldLineTracer()
    #     with w7x.distribute(True):
    #         graph = flt.diffusion(start_points=n_start)
    #         state = graph.compute()
    #     state.save(state_path)
    state = w7x.State.load(state_path)

    components = w7x.simulation.components.Components()
    divertor_component = w7x.config.AssemblyGroups.Divertor().components[0]
    assert divertor_component.id == 165
    state = components.set_meshes(state, component=divertor_component)
    mesh = state.pwi.power_density(divertor_component)

    # The mesh object gives acess to the number of hits
    print("N hits on this divertor in total:", sum(mesh.maps[3].fields[0]))
    # The component_load object gives acess to the number of hits per area. Crosscheck:
    #     (Note: calculating mesh.triangles().areas() is expensive.)
    # print("Crosscheck with n/A * A", sum(mesh.maps[3].fields[1] * mesh.triangles().areas()))

    # for fast plotting with opengl we use the pyqtgraph backend of rna.plotting
    # rna.plotting.use("pyqtgraph")  # If you cancel this line, you will plot with matplotlib

    # show the heat load pattern (also available with an opencv backend for fast display)
    mesh.plot(dim=2, map_index=1, cmap="PiYG", vmax=1e-6)
    rna.plotting.show()

    # Realize that this image is quite noisy. map_index=1 would not be visual with vmax=max
    # since dividing hits on very small areas will give huge outliers.


if __name__ == "__main__":
    test_power_density_example()
