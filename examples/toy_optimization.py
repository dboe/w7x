"""
Explore simple state optimization with Hyperopt.
"""

import logging
import numpy as np
import pytest
import w7x
from w7x.simulation.vmec import Vmec
from w7x.lib.profiles import TwoPower

LOGGER = logging.getLogger(__name__)


def test_toy_optimization_example():
    # skip test if hyperopt is not installed
    hyperopt = pytest.importorskip("hyperopt")

    logging.basicConfig(level=logging.INFO)
    #  Use a VMEC local runner
    vmec = Vmec(strategy="local")

    #  Define the optimization domain
    space = {
        "I1": hyperopt.hp.uniform("I1", 13000, 14000),
        "p0": hyperopt.hp.uniform("p0", 1e5, 2e5),
    }

    #  Define the toy objective function to optimize for
    #  We allow changes of I1 and the value of the pressure on axis
    def fn(args):
        I1, p0 = args.values()
        LOGGER.info(f"Try with I1={I1:5.0f} and p0={p0:5.4e}")

        currents = [I1 * c for c in w7x.config.MagneticConfig.standard_rw]
        state = w7x.State(
            w7x.config.CoilSets.Ideal.from_currents(*currents, unit="A"),
            w7x.config.Plasma.Vacuum(
                pressure_profile=TwoPower(coefficients=[p0, 1, 2], domain=[0, 1])
            ),
            w7x.config.Equilibria.InitialGuess(),
        )
        #  Use reduced force tolerance levels to converge quicker
        state = vmec.free_boundary(
            state, force_tolerance_levels=[1e-3, 1e-5, 1e-9, 1e-10]
        )

        #  Define target beta and magnetic field on axis (phi=0)
        target_beta = 0.02
        target_b_axis = 2.52

        #  Build loss value
        loss = None
        status = hyperopt.STATUS_FAIL
        if state.equilibrium.beta is not None:
            loss = np.abs(state.equilibrium.beta - target_beta) / target_beta
            loss += np.abs(state.equilibrium.b_axis - target_b_axis) / target_b_axis
            status = hyperopt.STATUS_OK

            LOGGER.info(
                f"Achieved beta of beta={state.equilibrium.beta:5.4f} "
                f"and b_axis={state.equilibrium.b_axis:5.4f}"
            )

        #  Return tuple of loss, status and state
        return {"loss": loss, "status": status, "state": state}

    #  You could provide a list of educated guess to try first
    educated_guess = [{"I1": 13067, "p0": 1.3e5}]

    best = hyperopt.fmin(
        fn,
        space=space,
        algo=hyperopt.tpe.suggest,
        points_to_evaluate=educated_guess,
        max_evals=5,
    )

    #  Check the best values
    #  TODO-2(@amerlo): add more info on the state
    #  TODO-2(@amerlo): add plot of trials
    print(best)


if __name__ == "__main__":
    test_toy_optimization_example()
