#!/usr/bin/env
"""
tracing the line of sight to one phi in the machine

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import matplotlib.pyplot as plt
import numpy as np
import rna.plotting
import tfields
import w7x
from w7x.simulation.flt import FieldLineTracer


def test_trace_line_of_sight_example():
    # define phi
    phi = 171.2 / 180 * np.pi
    flt = FieldLineTracer()

    # define thomson and pellet injection line of sight
    los_ts_out = (-6.222769, 1.04372, -0.126459)
    los_ts_in = (-4.360587, 0.590618, 0.482721)
    los_pellet_out = (-4.021, -4.786927, 0.401956)
    los_pellet_in = (-3.414576, -3.853587, 0.236581)
    n = 100  # how many points you want to plot
    los_ts = tfields.Points3D(
        np.array(
            [
                np.linspace(los_ts_in[0], los_ts_out[0], n),
                np.linspace(los_ts_in[1], los_ts_out[1], n),
                np.linspace(los_ts_in[2], los_ts_out[2], n),
            ]
        ).T
    )
    los_pellet = tfields.Points3D(
        np.array(
            [
                np.linspace(los_pellet_in[0], los_pellet_out[0], n),
                np.linspace(los_pellet_in[1], los_pellet_out[1], n),
                np.linspace(los_pellet_in[2], los_pellet_out[2], n),
            ]
        ).T
    )

    # trace each point of the line of sight to the chosen phi
    tom = flt.line_phi(points=los_ts, phi=phi)
    pellet = flt.line_phi(points=los_pellet, phi=phi)

    # plotting
    rna.plotting.set_style()
    axes = plt.gca()
    axes.grid(color="lightgrey")
    poincare_figure = w7x.plotting.poincare.Poincare()
    poincare_figure.plot(
        [],
        rmin=4.4,
        rmax=6.2,
        zmin=-0.5,
        zmax=1,
    )
    # calculate poincare surfaces of background field
    with w7x.stateful(False):
        traces = flt.trace_poincare()
        surfaces = traces.poincare_surfaces
    poincare_figure.plot_poincare_surfaces(
        surfaces,
        phi=phi,
        axes=axes,
        color="b",
    )
    poincare_figure.plot_poincare_surfaces(
        [tom],
        axes=axes,
        color="r",
        marker="o",
        s=16,
    )
    poincare_figure.plot_poincare_surfaces(
        [pellet],
        axes=axes,
        color="purple",
        marker="P",
        s=16,
    )
    rna.plotting.save("./thomson_pellet_los", extension=["png", "pgf", "pdf"])
    plt.show()


if __name__ == "__main__":
    test_trace_line_of_sight_example()
