"""
Be careful!!! I am only sure about r_vec on the z=0 plane in phi=0! So use only collocation points
on z=0, phi=0 first... However, B_r is close to 0 there as it is (by definition) orthongonal to the
surfaces. Lets talk about it on Monday. I think it has big flaws and is too naive...
"""
import tfields
from w7x.simulation.flt import FieldLineTracer


def test_field_line_pitch_example():
    flt = FieldLineTracer()

    # axis = flt.find_axis_at_phi(phi=0)
    axis = tfields.Points3D([[5.94858449e00, 0.00000000e00, 1.09156488e-06]])
    coll_point = tfields.Points3D([[5.4, 0, -1]], coord_sys=tfields.bases.CYLINDER)
    # n_points = 20
    # coll_point = tfields.Points3D(np.array([np.linspace(6, 6.3, n_points),
    #                                         np.linspace(0, 0, n_points),
    #                                         np.linspace(0, 0, n_points)]
    #                                        ).T,
    #                               coord_sys=tfields.bases.CYLINDER)
    coll_point.transform(tfields.bases.CARTESIAN)
    r_vec = (coll_point - axis).normalized()
    b_field = flt.magnetic_field(points=coll_point)
    b_r = b_field.t.dot(r_vec)
    print("B_r @ collocation points", b_r)
    print("Field line pitch at collocation points", b_r / b_field.norm())


if __name__ == "__main__":
    test_field_line_pitch_example()
