"""
Example on how to search for a specific beta with VMEC.
"""

import logging
import rna.log

import w7x
from w7x.lib.profiles import TwoPower
from w7x.simulation.vmec import Vmec

logging.basicConfig(
    handlers=[rna.log.TerminalHandler(level=logging.DEBUG)], level=logging.DEBUG
)


def test_vmec_beta_search():
    #  Start with a vacuum case
    state = w7x.State(
        w7x.config.CoilSets.Ideal.from_currents(
            *w7x.config.MagneticConfig.standard_rw, unit="rw"
        ),
        w7x.config.Plasma.Vacuum(
            pressure_profile=TwoPower(coefficients=[1e-6, 1, 2], domain=[0, 1])
        ),
        w7x.config.Equilibria.InitialGuess(),
    )

    #  Define target beta, magnetic field on axis (phi=0) and plasma volume
    beta = 0.02
    b_axis = 2.52
    vol = 30

    #  Search, reduce force_tolerance_levels to converge faster
    vmec = Vmec(strategy="local")
    state = vmec.search(
        state,
        beta=beta,
        b_axis=b_axis,
        vol=vol,
        force_tolerance_levels=[1e-3, 1e-5, 1e-9, 1e-12],
    )


if __name__ == "__main__":
    test_vmec_beta_search()
