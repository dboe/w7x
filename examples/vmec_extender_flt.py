# pylint:disable=abstract-class-instantiated
"""
Example on how to run VMEC, EXTENDER and run the field line tracer to show the field in poincare
representation.
"""
import argparse

import rna.parsing
import tfields
import w7x
import w7x.simulation.vmec
import w7x.simulation.extender
import w7x.simulation.flt
import w7x.simulation.components


def test_vmec_extender_flt_example():
    """
    Build a graph and compute it.
    """
    parser = argparse.ArgumentParser(parents=[rna.parsing.LogParser()])
    parser.add_argument("-n", type=int, default=4, help="number of cores")
    parser.add_argument(
        "--distribute", "-d", action="store_true", help="distribute the work load"
    )
    args = parser.parse_args()
    tmp_dir = w7x.config.general.tmp_dir

    #  Define a default state
    state = w7x.State(
        w7x.config.Assemblies.Pfcs(),
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
    )
    phi = 0

    #  Pick a backend: `web_service`, `local`, `slurm` or even `nn`
    vmec = w7x.simulation.vmec.Vmec(strategy="web_service")
    extender = w7x.simulation.extender.Extender(strategy="web_service")
    flt = w7x.simulation.flt.FieldLineTracer(strategy="web_service")
    components = w7x.simulation.components.Components()

    with w7x.distribute(args.distribute):
        # Compute a free boundary equilibrium
        graph = vmec.free_boundary(state)
        # Extend the equilibrium
        graph = extender.field(graph)
        # Evaluate the field by tracing the poisson surfaces
        graph = flt.trace_poincare(
            graph,
            phi=phi,
            seeds=tfields.Points3D(
                [[5.8, 0.0, 0.0], [5.85, 0.0, 0.0], [5.9, 0.0, 0.0], [6.0, 0.0, 0.0]]
            ),
            n_points=100,
        )
        graph = w7x.State.merged(
            graph,
            components.mesh_slice(state, phi=phi),
        )

        if args.distribute:
            graph.visualize(filename=tmp_dir + "/" + "vmec_extender_flt_graph.pdf")
            state = w7x.compute(
                graph,
                cache=tmp_dir + "/" + "vmec_extender_flt_example_state.dill",
                scheduler=dict(n_workers=args.n),
            )
        else:
            state.save(tmp_dir, "vmec_extender_flt_example_state_non_distributive.dill")


if __name__ == "__main__":
    test_vmec_extender_flt_example()
