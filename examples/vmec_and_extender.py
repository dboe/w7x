"""
Example on how to get the total field from a VMEC run.

Make sure you have set the correct path in the `~/.w7x/config.cfg` file.
"""

import logging

import numpy as np
import tfields

import w7x
from w7x.lib.profiles import TwoPower, PowerSeries
from w7x.simulation.vmec import Vmec
from w7x.simulation.extender import Extender

logging.basicConfig(level=logging.INFO)


def test_vmec_and_extender_example():
    state = w7x.State(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
    )

    # Define magnetic configuration
    pressure = TwoPower(coefficients=[1e5, 1, 2])
    phi_edge = -2.0

    state = w7x.State(
        w7x.config.CoilSets.Ideal.from_currents(
            *w7x.config.MagneticConfig.ejm_252, unit="A"
        ),
        w7x.config.Plasma.Vacuum(pressure_profile=pressure),
        w7x.config.Equilibria.InitialGuess(phi=PowerSeries(coefficients=[0, phi_edge])),
    )

    # Compute a free boundary equilibrium first
    vmec = Vmec()
    state = vmec.free_boundary(state)

    extender = Extender()
    state = extender.field(
        state,
        points=tfields.Points3D(
            [[6.222769, np.pi / 5, -0.126459]], coord_sys=tfields.bases.CYLINDER
        ),
    )

    # Save the state for later working with it
    state.save("extender_vmec_case.dill")

    # Inspect the computed field
    field = state.equilibrium.field

    print(
        f"Magnetic field vector at r, phi, z = {field[0]} is {field.fields[0][0]} [T]"
    )


if __name__ == "__main__":
    test_vmec_and_extender_example()
