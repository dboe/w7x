#!/usr/bin/env
"""
Retrieving the angle of magnetiv field and surface normal.
There are sophisticated methods to retrieve the normal on a PFC
within this framework. Ask the author if required.

Author: daniel.boeckenhoff@ipp.mpg.de
"""
##################################################

import logging
import numpy as np

import tfields
import w7x
import w7x.simulation.flt

logging.basicConfig(level=logging.DEBUG)


LOGGER = logging.getLogger(__name__)


# pylint:disable=pointless-string-statement,protected-access,abstract-class-instantiated
def test_angle_pfc_field_line_example():
    """DO YOUR DEFINITIONS HERE!"""

    coil_set = w7x.config.CoilSets.Deformed.from_currents(
        *w7x.config.MagneticConfig.high_iota_rw, unit="rw"
    )
    # Example for a different magnetic configuration
    # magnetic_config = w7x.flt.Run().magnetic_config  # standard configuration
    point = tfields.Points3D([[-6.222769, 1.04372, -0.126459]])
    surface_normal = tfields.Points3D([[2.0, 0.0, 0.0]])

    """ get the magnetic field """
    magnetic_field = w7x.simulation.flt.FieldLineTracer().magnetic_field(
        w7x.State(coil_set), points=point
    )

    """ calculate the angle """
    magnetic_field_norm = magnetic_field / np.linalg.norm(magnetic_field)
    surface_normal_norm = surface_normal / np.linalg.norm(surface_normal)
    angle = np.arccos(
        np.clip(np.dot(magnetic_field_norm[0], surface_normal_norm[0]), -1.0, 1.0)
    )

    logging.info("Angle in between magnetic_field and surface normal: %s rad", angle)


if __name__ == "__main__":
    test_angle_pfc_field_line_example()
