"""
diffuse a extended vmec run and save the result
This example assumes, magnetic field file is already existing at <afs_path>.
"""
import argparse
import numpy as np

import rna
import rna.plotting
import tfields
import w7x
from w7x.simulation.flt import FieldLineTracer


def test_poincare_equilibrium_example():
    """
    For parser options, use the --help flag at call time
    """
    parser = argparse.ArgumentParser(parents=[rna.parsing.LogParser()])
    _ = parser.parse_args()

    # VMEC + EXTENDER standard case (0 current, 0 pressure)
    # path = w7x.config.extender.test.example_dat
    # mgrid = w7x.model.MGrid(
    #     path=path,
    #     base_vectors=((3.81, 6.87, 173j), (0, 2 * np.pi, 280j), (-1.45, 1.45, 170j)),
    # )
    path = w7x.config.extender.test.example_datc
    mgrid = w7x.model.MGrid(
        path=path,
    )
    state = mgrid.to_state()

    flt = FieldLineTracer()

    """poincare plot creation"""
    phi = 171.2 / 180 * np.pi

    with w7x.distribute(True):
        graph = flt.trace_poincare(
            state,
            phi=phi,
            seeds=tfields.Points3D([[5.8, 0.0, 0.0], [6.0, 0.0, 0.0]]),
            n_points=100,
        )
        state = w7x.compute(
            graph
        )  # , cache=w7x.config.general.tmp_dir + "/poincare_equilibrium.dill")

    # plotting
    rna.plotting.set_style()
    poincare_figure = w7x.plotting.Poincare()
    poincare_figure.gca().grid(color="lightgrey")
    poincare_figure.plot(state, phi=phi)
    poincare_figure.show()


if __name__ == "__main__":
    test_poincare_equilibrium_example()
