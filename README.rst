library documentation
=====================

..
    [shields-start]
.. pypi
.. image:: https://img.shields.io/pypi/v/w7x.svg
    :target: https://pypi.python.org/pypi/w7x
.. ci
.. image:: https://gitlab.mpcdf.mpg.de/dboe/w7x/badges/master/pipeline.svg
    :target: https://gitlab.mpcdf.mpg.de/dboe/w7x/-/pipelines/latest
.. latest release
.. image:: https://gitlab.mpcdf.mpg.de/dboe/w7x/-/badges/release.svg
    :target: https://gitlab.mpcdf.mpg.de/dboe/w7x/-/releases
.. coverage
.. image:: https://gitlab.mpcdf.mpg.de/dboe/w7x/badges/master/coverage.svg
    :target: https://gitlab.mpcdf.mpg.de/dboe/w7x/commits/master
.. docs
.. image:: https://img.shields.io/website-up-down-green-red/https/dboe.pages.mpcdf.de/w7x.svg?label=docs
    :target: https://dboe.pages.mpcdf.de/w7x
.. pre-commit
.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit
..
    [shields-end]

Overview
--------

..
    [overview-start]

The `w7x` package is a **simulation framework** that defines **interfaces for simulation codes** with interchangeable **backends**, **computation pipelines** in a directed acyclic **graph**.
It provides access to simulation codes that **does not require the user to be a domain expert** of the specific codes and **will take care** about particularities (like binary installation, sanity checks, output files/format, passing on the output to follow-on codes etc).

The package is named after the Wendelstein 7-X stellarator, but it is **generic** enough to be used for any **magnetic confinement fusion** device.

..
    [overview-end]

Licensed under the ``MIT License``

Resources
~~~~~~~~~

..
    [resources-start]

- Documentation: https://dboe.pages.mpcdf.de/w7x
- Source code: https://gitlab.mpcdf.mpg.de/dboe/w7x
- Examples: https://gitlab.mpcdf.mpg.de/dboe/w7x/-/tree/master/examples
- Bug reports: https://gitlab.mpcdf.mpg.de/dboe/w7x/-/issues/
- Pypi: https://pypi.python.org/pypi/w7x

..
    [resources-end]
