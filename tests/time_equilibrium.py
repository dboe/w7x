"""
Profiling script for the equilibrium package functions.
"""

import cProfile

import numpy as np

from w7x.lib.equilibrium import Cos

coef = np.arange(99 * 12 * 25).reshape(99, 12, 25)

#  Define axis for (s, theta, phi)
ss = np.linspace(0, 1, num=50, endpoint=True)
thetas = np.linspace(0, 2 * np.pi, num=36)
phis = np.linspace(0, 2 * np.pi / 5, num=18)

#  Build list of points
xs = [(s, t, p) for s in ss for t in thetas for p in phis]
xs_ids = [(int(s * 98), t, p) for s in ss for t in thetas for p in phis]
assert len(xs) == len(ss) * len(thetas) * len(phis)

cos = Cos(coef, num_field_periods=5)


def packed():
    cos(xs)


def unpacked():
    for x in xs:
        cos(x)


def unpacked_static():
    for _, theta, phi in xs_ids:
        Cos._call(
            theta,
            phi,
            coef[0],  # just use the first surface
            np.arange(0, 12, dtype=int),
            np.arange(-12, 13, dtype=int),
            5,
        )


def linspace():
    cos.linspace(ns=50, ntheta=36, nphi=18)


if __name__ == "__main__":
    print("Compute full flux surface with numpy vectorize\n")
    cProfile.run("packed()", sort="tottime")

    print("Compute flux surface with numpy vectorize and loop\n")
    cProfile.run("unpacked()", sort="tottime")

    print("Compute flux surface with static method and loop\n")
    cProfile.run("unpacked_static()", sort="tottime")

    print("Compute flux surface with linspace\n")
    cProfile.run("linspace()", sort="tottime")
