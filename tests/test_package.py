#!/usr/bin/env python

"""Tests for `w7x` package."""

import unittest

import w7x


class TestPackage(unittest.TestCase):
    """Tests for `w7x` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""
        self.assertIsInstance(w7x.__version__, str)
