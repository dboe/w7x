import w7x
import w7x.simulation.vmec
import pytest


@pytest.fixture
def state():
    path = w7x.config.vmec.test.example_wout_nc
    wout = w7x.simulation.vmec.Wout.load(path=path)
    file_id = w7x.config.vmec.test.example_vmec_id
    assert wout.file_id == file_id
    return wout.to_state()


@w7x.node
@w7x.dependencies(
    w7x.model.Equilibrium, w7x.model.CoilSet, return_me=w7x.dependencies.REQUIRED
)
def compute_state(state, **kwargs):
    return_me = kwargs.pop("return_me")
    return return_me


def check_link(link, state):
    assert link is not None
    assert link.ref in state.resources._children


def test_link_attribute(state):
    vac_field_link = state.equilibrium.get_link("vacuum_field")
    check_link(vac_field_link, state)


def test_merged_links(state):
    merged_state = w7x.State.merged(state)
    vac_field_link = merged_state.equilibrium.get_link("vacuum_field")
    check_link(vac_field_link, state)


@pytest.mark.parametrize("distribute", [True, False])
def test_node_dependencies_links(state, distribute):
    with w7x.distribute(distribute):
        # dont pass a state and use the dependencies
        graph = compute_state(return_me=state)
        computed_state = w7x.compute(graph)
    vac_field_link = computed_state.equilibrium.get_link("vacuum_field")
    check_link(vac_field_link, state)


@pytest.mark.parametrize("distribute", [True, False])
def test_initial_state_links(state, distribute):
    with w7x.distribute(distribute):
        graph = compute_state(state, return_me=w7x.model.Equilibrium(kinetic_energy=42))
        computed_state = w7x.compute(graph)
    assert computed_state.equilibrium.kinetic_energy == 42
    vac_field_link = computed_state.equilibrium.get_link("vacuum_field")
    check_link(vac_field_link, state)
