import dataclasses
import numpy as np
import unittest
import rna.pattern.link
import w7x
import w7x.simulation.vmec
from .test_basics import SA3


def array_safe_eq(a, b) -> bool:
    """Check if a and b are equal, even if they are numpy arrays"""
    if a is b:
        return True
    if not isinstance(a, type(b)):
        return False
    if isinstance(a, np.ndarray):
        return a.shape == b.shape and (a == b).all()
    if isinstance(a, (tuple, list)):
        return all([array_safe_eq(ai, bi) for ai, bi in zip(a, b)])
    if isinstance(a, rna.pattern.link.Link):
        return a.ref == b.ref
    if dataclasses.is_dataclass(a):
        return dc_eq(a, b)
    try:
        return a == b
    except TypeError:
        return NotImplementedError()


def dc_eq(dc1, dc2) -> bool:
    """checks if two dataclasses which hold numpy arrays are equal"""
    if dc1 is dc2:
        return True
    if dc1.__class__ is not dc2.__class__:
        return NotImplementedError()  # better than False

    #  Hack to do not consider non-pickable fields in dataclas astuple().
    #  Similar to previous w7x.compatibility.dataclass_fields
    #  Here we want to avoid the data field of a File object, which could
    #  be non-pickable (e.g., netCDF4.Dataset).

    fields = dataclasses.fields

    def dataclass_fields(expr):
        """
        Hack to remove not-pickable fields from dataclas tuple of fields.
        """
        ret = []
        for field in fields(expr):
            if field.name != "data":
                ret.append(field)
        return tuple(ret)

    dataclasses.fields = dataclass_fields

    with w7x.exposed(True):
        t1 = dataclasses.astuple(dc1)
        t2 = dataclasses.astuple(dc2)

    dataclasses.fields = fields

    return all(array_safe_eq(a1, a2) for a1, a2 in zip(t1, t2))


class TestIo(unittest.TestCase):
    def setUp(self):
        vmec_id = w7x.config.vmec.test.example_vmec_id

        stdcase_vacuum_wout = rna.path.resolve(
            w7x.__file__,
            "../../tests/resources/vmec",
            "wout_" + vmec_id + ".nc",
        )
        wout = w7x.simulation.vmec.Wout.load(
            path=stdcase_vacuum_wout,
        )
        resources = w7x.model.Resources()
        resources.add(wout)

        wout_state = wout.to_state()
        self.equi, self.resources = wout_state.equilibrium, wout_state.resources
        self.coils = w7x.config.CoilSets.Ideal.from_currents(
            *w7x.config.MagneticConfig.high_mirror_rw  # Note: vmec=standard - just test
        )
        self.assembly = w7x.config.Assemblies.PfcsOp11()

        state = w7x.State.merged(self.equi, self.coils, self.assembly, self.resources)
        self.state = state
        # dill issues with MetaClass make some objects fail for saving in dill. Watch this:
        # mmkuqfoundation/dill/issues/332
        # which is maybe resolved by this:
        # https://github.com/uqfoundation/dill/pull/450
        self.path = "~/tmp/w7x_obj_plane_test.dill"

        initial_guess = w7x.config.Equilibria.InitialGuess(
            vacuum_field=rna.pattern.link.Link(
                ref=w7x.model.MGrid(path="mgrid_w7x_nv36_hires.nc"),
                fget=w7x.model.MGrid.to_numpy,
            )
        )
        # initial_guess.vacuum_field = 5
        self.objects = [
            SA3(my_attr=5),  # the most simple object
            self.coils.get_coils(flat=True)[0],  # a leaf
            self.coils,  # a composite
            self.assembly,  # another composite
            w7x.lib.profiles.PowerSeries(),  # a Profile
            w7x.model.Equilibrium(),  # a model with many empty fields
            initial_guess.flux_surfaces,  # A TensorSeries
            w7x.model.Equilibrium(
                vacuum_field=initial_guess.get_ref("vacuum_field")
            ),  # A link
            initial_guess,  # a densely populated leaf
            self.equi,  # a large leaf
            self.state,
        ]

    def check_parents(self, obj, parent=0):
        if not isinstance(obj, w7x.state.StateComponent):
            return
        if parent != 0:
            self.assertIs(obj.parent, parent)
        if obj.is_composite():
            for leaf in obj._children:
                self.check_parents(leaf, obj)

    def test_write_read(self):
        for obj in self.objects:
            # TODO(@dboe): check whats wrong with coils?
            # self.check_parents(obj)  # before and after saving parents are set properly
            self.assertTrue(dc_eq(obj, obj))

            obj.save(self.path)
            obj_read = type(obj).load(self.path)

            with w7x.exposed(True):
                self.assertTrue(dc_eq(obj, obj_read))
            # self.check_parents(obj)


class TestPickle(TestIo):
    def setUp(self):
        super().setUp()
        self.path = "~/tmp/w7x_obj_plane_test.pickle"
