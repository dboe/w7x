# pylint:disable=missing-module-docstring,missing-class-docstring,missing-function-docstring,too-few-public-methods,protected-access,unused-argument
"""
State tests
"""
import unittest
import dataclasses
import dask

import tfields
import w7x


class SA1(w7x.core.StateLeaf):
    """Dummy State Attribute"""


@dataclasses.dataclass
class SA2(SA1):
    """Dummy State Attribute"""

    my_attr: int = 3


class SA3(SA2):
    """Dummy State Attribute"""


class TestStateLeaf(unittest.TestCase):
    def setUp(self):
        self.obj = SA3(my_attr=5)

    def test_attribute_parents(self):
        self.assertTupleEqual(self.obj._attribute_parents(), (SA2, SA1))

    def test_origin(self):
        self.assertIs(self.obj.origin(), SA1)

    def test_fields(self):
        thing = self.obj
        self.assertListEqual([f.name for f in dataclasses.fields(thing)], ["my_attr"])


class TestBasicState(unittest.TestCase):
    def test_origin(self):
        state = w7x.State()
        self.assertIs(state.origin(), w7x.State)

    def test_leaf_parent_is_state(self):
        state = w7x.State()
        sa = SA3(my_attr=42)
        state.add(sa)
        self.assertIs(sa.parent, state)
        state.remove(sa)

    def test_state_parent_is_none(self):
        state = w7x.State()
        self.assertIs(state.parent, None)

    def test_set_default(self):
        # With type
        state = w7x.State()
        state.set_default(SA3)
        self.assertTrue(state.has(SA1))
        self.assertEqual(state.s_a1.my_attr, 3)

        # With object
        state = w7x.State()
        state.set_default(SA3())
        self.assertTrue(state.has(SA1))
        self.assertEqual(state.s_a1.my_attr, 3)

    def test_merged(self):
        # With object
        state = w7x.State()
        state = state.merged(SA3())
        self.assertTrue(state.has(SA1))
        self.assertEqual(state.s_a1.my_attr, 3)

        # Test Update
        sa3 = SA3(my_attr=5)
        state = state.merged(sa3)
        self.assertEqual(state.s_a1.my_attr, 5)


class TestState(unittest.TestCase):
    def test_merged_component(self):
        initial_guess = w7x.config.Equilibria.InitialGuess(kinetic_energy=0)
        equi = w7x.model.Equilibrium.merged(
            initial_guess,
            w7x.model.Equilibrium(),
            w7x.model.Equilibrium(plasma_volume=30.0),
            w7x.model.Equilibrium(kinetic_energy=42.0),
            w7x.model.Equilibrium(),
        )
        self.assertEqual(equi.plasma_volume, 30)
        self.assertEqual(equi.kinetic_energy, 42)
        self.assertEqual(initial_guess.kinetic_energy, 0)
        self.assertFalse(hasattr(equi, "pk"))

    def test_merged_composite(self):
        pwi_f = w7x.model.Pwi(origin_points=tfields.Points3D([[1, 2, 3]]))
        pwi_f.add(
            w7x.model.PlasmaComponentInteraction(
                component_index=12,
                inverse_field=True,
                origin_point_indices=[0],
                connection_lengths=tfields.Tensors([10]),
            )
        )
        pwi_b = w7x.model.Pwi(origin_points=tfields.Points3D([[1, 2, 3]]))
        pwi_b.add(
            w7x.model.PlasmaComponentInteraction(
                component_index=3,
                inverse_field=False,
                origin_point_indices=[0],
                connection_lengths=tfields.Tensors([32]),
            )
        )

        pwi = w7x.model.Pwi.merged(pwi_f, pwi_b)
        self.assertEqual(len(pwi.components), 2)
        self.assertTrue(pwi.components[0].inverse_field)
        self.assertFalse(pwi.components[1].inverse_field)
        self.assertEqual(pwi.connection_lengths().fields[0][0], 42)
        self.assertEqual(
            len(pwi.connection_lengths()), len(pwi.connection_lengths().fields)
        )

    def test_merged_state_with_delayed_attribute(self):
        @w7x.node
        def get_plasma():
            plasma = w7x.config.Plasma.Vacuum()
            plasma.diffusion_coeff = 4.2
            return plasma

        state = w7x.State.merged(
            w7x.model.PlasmaParameters(), w7x.model.Equilibrium(kinetic_energy=42)
        )
        plasma = w7x.config.Plasma.Vacuum()
        plasma.diffusion_coeff = 4.2

        with w7x.distribute():
            plasma = get_plasma()
            graph = w7x.State.merged(state, plasma)
            result_state = graph.compute()
            self.assertTrue(result_state.has(w7x.model.PlasmaParameters))
            self.assertTrue(result_state.has(w7x.model.Equilibrium))
            self.assertEqual(result_state.plasma_parameters.diffusion_coeff, 4.2)
            self.assertEqual(result_state.equilibrium.kinetic_energy, 42)

    def test_merged_state_distributed(self):
        @w7x.node
        def get_equi():
            equi = w7x.config.Equilibria.InitialGuess()
            equi.kinetic_energy = 10.0
            return equi

        @w7x.node
        def get_plasma():
            plasma = w7x.config.Plasma.Vacuum()
            plasma.diffusion_coeff = 4.2
            return plasma

        @w7x.node
        def delta_e(a, b):
            return a.equilibrium.kinetic_energy - b.equilibrium.kinetic_energy

        state = w7x.State.merged(
            w7x.config.Plasma.Vacuum(),
            w7x.config.Equilibria.InitialGuess(kinetic_energy=52),
        )

        with w7x.distribute():
            plasma = get_plasma()
            state = w7x.State.merged(state, plasma)
            result_state = state.compute()
            self.assertTrue(result_state.has(w7x.model.PlasmaParameters))
            self.assertTrue(result_state.has(w7x.model.Equilibrium))
            equi = get_equi()
            de = delta_e(state, w7x.State.merged(equi)).compute()
            state1 = w7x.State.merged(state, equi)
            de1 = delta_e(state1, w7x.State.merged(equi)).compute()

            # explicit state
            state2 = w7x.State.merged(
                w7x.config.Equilibria.InitialGuess(kinetic_energy=31)
            )
            de2 = delta_e(state2, w7x.State.merged(equi)).compute()

        self.assertEqual(de, 42)
        self.assertEqual(de1, 0)
        self.assertEqual(de2, 21)

    def test_merged_state_delayed(self):
        def fun(state):
            plasma = w7x.model.Equilibrium(plasma_volume=20.0)
            return w7x.State.merged(state, plasma)

        state = w7x.State(
            w7x.model.Equilibrium(plasma_volume=30.0),
        )
        fun = dask.delayed(fun)(state)
        state = fun.compute(scheduler="single-threaded")
        self.assertEqual(state.equilibrium.plasma_volume, 20)
        self.assertFalse(hasattr(state.equilibrium, "pk"))

    def test_merged_state_dependencies(self):
        @w7x.dependencies(w7x.config.Plasma.Vacuum, ...)
        def fun_returns_plasma(state):
            return w7x.model.PlasmaParameters(bootstrap_current=40)

        fun = dask.delayed(fun_returns_plasma)()
        state = fun.compute()
        self.assertEqual(state.plasma_parameters.bootstrap_current, 40)
        self.assertFalse(hasattr(state.plasma_parameters, "pk"))

    def test_merged(self):
        plasma = w7x.model.Equilibrium()
        state = w7x.State(plasma)
        state = w7x.State.merged(state)
        self.assertTrue(state.has(w7x.model.Equilibrium))

        state = w7x.State.merged(
            state,
            w7x.model.Equilibrium(plasma_volume=30.0),
            w7x.model.Equilibrium(kinetic_energy=42.0),
        )
        self.assertEqual(
            [c.attribute_name() for c in state._children].count("equilibrium"), 1
        )
        self.assertEqual(state.equilibrium.plasma_volume, 30.0)
        self.assertEqual(state.equilibrium.kinetic_energy, 42.0)
