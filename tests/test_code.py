import unittest
import abc

import w7x
from w7x.core import Backend


class FooBackend(Backend):
    @staticmethod
    @abc.abstractmethod
    def _compute(state, **kwargs):
        pass


class FooBackendA(FooBackend):
    @staticmethod
    def _compute(state, **kwargs):
        return "foo A"


class FooBackendB(FooBackend):
    @staticmethod
    def _compute(state, **kwargs):
        return "foo B"


class Foo:  # (Code):
    STRATEGY_Type = FooBackend
    STRATEGY_DEFAULT = FooBackend

    def __init__(self):
        self.backend = None

    @w7x.node()
    @w7x.dependencies(...)
    def compute(self, state, **kwargs):
        return self.backend._compute(state, **kwargs)


class TestCode(unittest.TestCase):
    def setUp(self):
        w7x.stateful.disable()

    def tearDown(self):
        w7x.stateful.enable()

    def test_backend_persistents(self):
        foo_a = Foo()
        foo_a.backend = FooBackendA()
        res_a = foo_a.compute()
        self.assertEqual(res_a, "foo A")

        foo_b = Foo()
        foo_b.backend = FooBackendB()
        res_b = foo_b.compute()
        self.assertEqual(res_b, "foo B")

        self.assertIsNot(foo_a, foo_b)
        self.assertIsNot(foo_a.backend, foo_b.backend)

    def test_backend_persistents_same_frontend(self):
        foo = Foo()
        foo.backend = FooBackendA()
        res_a = foo.compute()
        id_a = id(foo.backend)
        self.assertEqual(res_a, "foo A")
        foo.backend = FooBackendB()
        res_b = foo.compute()
        id_b = id(foo.backend)
        self.assertEqual(res_b, "foo B")

        self.assertNotEqual(id_a, id_b)
