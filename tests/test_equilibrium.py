"""
Test for equilibrium objects.
"""

import copy
import unittest
import numpy as np

import tfields
from w7x.lib.equilibrium import Cos, Sin, Fourier, TensorSeries, interp


class TestFourierTerm(unittest.TestCase):
    coef = np.array([[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]]])

    def test_init_none_coef(self):
        self.assertRaises(TypeError, Cos, None)

    def test_from_coefficients_none_coef(self):
        self.assertRaises(TypeError, Cos.from_coefficients, None)

    def test_init_too_large_dimension(self):
        self.assertRaises(TypeError, Cos, [[[[1.0]]]])

    def test_init_even_toroidal_dimension(self):
        coef = np.arange(8).reshape(1, 2, 4)
        self.assertRaises(TypeError, Cos, coef)

    def test_init_shape(self):
        series = Cos(self.coef)
        self.assertEqual(series.shape, (1, 2, 3))

    def test_set_null(self):
        coef = [1.0, 1.0, -0.1]
        series = Cos.from_coefficients(coef, mpol=0, ntor=1)
        series.set_null()
        np.testing.assert_array_equal(series.coef, np.array([[[0.0, 1.0, -0.1]]]))

    def test_from_coefficients(self):
        coef = [0.0, 1.0, -0.1]
        series = Cos.from_coefficients(coef, mpol=2, ntor=0)
        np.testing.assert_array_equal(series.coef, np.array([[[0.0], [1.0], [-0.1]]]))

    def test_from_coefficients_3d_list(self):
        coef = [[[0.0, 1.0, -0.1]]]
        series = Cos.from_coefficients(coef, mpol=0, ntor=1)
        np.testing.assert_array_equal(series.coef, coef)

    def test_from_coefficients_3d_list_wrong_resolution(self):
        coef = [[[0.0, 1.0, -0.1]], [[0.0, 0.5, -0.2]]]
        self.assertRaises(TypeError, Cos.from_coefficients, coef, mpol=1, ntor=1)

    def test_from_coefficients_vmec_flux_surfaces(self):
        #  Use typical Fourier resolution used in VMEC
        coef = np.arange(99 * 288).reshape(99, 288)
        full_coef = np.zeros(99 * 300).reshape(99, 300)
        full_coef[:, 12:] = coef
        full_coef = full_coef.reshape(99, 12, 25)
        series = Cos.from_coefficients(coef, mpol=11, ntor=12)
        np.testing.assert_array_equal(series.coef, full_coef)

    def test_from_coefficients_vmec_nyquist_resolution(self):
        #  Use typical Nyquist Fourier resolution used in VMEC
        coef = np.arange(99 * 685).reshape(99, 685)
        full_coef = np.zeros(99 * 19 * 37).reshape(99, 19 * 37)
        full_coef[:, 18:] = coef
        full_coef = full_coef.reshape(99, 19, 37)
        series = Cos.from_coefficients(coef, mpol=18, ntor=18)
        self.assertTrue(np.array_equal(series.coef, full_coef))

    def test_from_coefficients_wrong_resolution(self):
        coef = [0.0, 1.0, -0.1]
        self.assertRaises(TypeError, Cos.from_coefficients, coef, mpol=1, ntor=0)

    def test_from_coefficients_vmec_reduced(self):
        coef = list(np.arange(30, dtype=float).flatten())  # s = 2, m = 2, n = 2
        vmec_coef = coef[2:15] + coef[17:]  # remove n < 0 coefficients
        series = Cos.from_coefficients(vmec_coef, mpol=2, ntor=2)
        self.assertListEqual(list(series.coef[0].flatten()), [0.0] * 2 + coef[2:15])
        self.assertListEqual(list(series.coef[1].flatten()), [0.0] * 2 + coef[17:])

    def test_truncated_poloidal_modes(self):
        series = Cos(self.coef)
        truncated = series.truncated(mpol=0)
        truncated_coef = self.coef[:, :1, :]
        np.testing.assert_array_equal(truncated.coef, truncated_coef)

    def test_truncated_larger_m(self):
        coef = np.arange(30, dtype=float)  # s = 2, m = 2, n = 2
        series = Cos.from_coefficients(list(coef.flatten()), mpol=2, ntor=2)
        truncated = series.truncated(mpol=3)
        np.testing.assert_array_equal(truncated.coef.flatten(), coef)

    def test_truncated_larger_n(self):
        coef = np.arange(30, dtype=float)  # s = 2, m = 2, n = 2
        series = Cos.from_coefficients(list(coef.flatten()), mpol=2, ntor=2)
        truncated = series.truncated(ntor=3)
        np.testing.assert_array_equal(truncated.coef.flatten(), coef)


class FourierTermTests:
    """
    Generic tests for all FourierTerm subclasses.
    """

    atol = 1e-2

    def test_call(self):
        series = self.get_series(self.coef, self.num_field_periods)
        for loc, value in self.test_locs_values:
            np.testing.assert_almost_equal(
                series(loc, atol=self.atol), value, err_msg=str((loc, value))
            )

    def test_call_s_out_of_range(self):
        series = self.get_series(self.coef, self.num_field_periods)
        locs = [[2, 0, 0], [-1, 0, 0]]
        for loc in locs:
            self.assertRaises(ValueError, series, loc)

    def test_call_atol_close_enough(self):
        coef = np.random.random(size=100 * 3).reshape(100, 1, 3)  # s=100, m=0, n=1
        series = self.get_series(coef, self.num_field_periods)
        #  Avoid 0 and 1 so not to go out of range
        locs = np.ones((98, 3))
        locs[:, 0] = np.linspace(0, 1, num=100)[1:-1]
        close_locs = copy.deepcopy(locs)
        close_locs[:, 0] = locs[:, 0] + np.random.random(size=98) * self.atol
        interp_values = series(close_locs, atol=self.atol)
        values = series(locs)
        #  Just check that interpolated values are different from on grid ones
        self.assertTrue((interp_values != values).all())

    def test_call_atol_not_close_enough(self):
        coef = np.arange(15).reshape(5, 1, 3)  # s=5, m=0, n=1
        series = self.get_series(coef, self.num_field_periods)
        locs = np.zeros((3, 3))
        locs[:, 0] = np.linspace(0, 1, num=5)[1:4]
        close_locs = copy.deepcopy(locs)
        close_locs[:, 0] = locs[:, 0] + np.random.random(size=3) * self.atol * 10
        self.assertRaises(ValueError, series, close_locs, atol=self.atol)

    def test_from_coefficients(self):
        coef = [0.0, 1.0, -0.1]
        series = self.get_series_from_coefficients(
            coef, mpol=2, ntor=0, num_field_periods=5
        )
        self.assertEqual(series.__class__.__name__, self.class_name)
        np.testing.assert_array_equal(series.coef, np.array([[[0.0], [1.0], [-0.1]]]))


class TestCos(FourierTermTests, unittest.TestCase):
    class_name = "Cos"
    num_field_periods = 5
    coef = [[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]], [[0.0, 1.0, -0.1], [-0.5, 0.1, 0.5]]]

    test_locs_values = [
        ([0, 0, 0], 1.1),
        ([1, 0, 0], 1.0),
        ([0.005, 0, 0], 1.1 * 0.995 + 1.0 * 0.005),  # linear interpolation
        ([0.995, 0, 0], 1.0 * 0.995 + 1.1 * 0.005),  # linear interpolation
        ([[0, 0, 0]], 1.1),
        ([[0, 0, 0], [0, 0, np.pi / 2 / 5]], [1.1, 1.2]),
        ([0, 0, np.pi / 2 / 5], 1.2),
        ([0, np.pi / 2, 0], 0.9),
        ([0, np.pi / 2, np.pi / 2 / 5], 2.0),
        (tfields.Points3D([[0, 0, 0]]), 1.1),
        (tfields.Points3D([[0, 0, 0], [0, 0, np.pi / 2 / 5]]), [1.1, 1.2]),
    ]

    @staticmethod
    def get_series(*args, **kwargs):
        return Cos(*args, **kwargs)

    @staticmethod
    def get_series_from_coefficients(*args, **kwargs):
        return Cos.from_coefficients(*args, **kwargs)

    ####################
    # CLASS ONLY TESTS #
    ####################

    def test_on_axis(self):
        #  Mimic computation of magnetic field on axis at phi = 0.0
        #  m = 4, n = 1
        coef = [0.0, 0.0, 0.0, 5.9, -0.3, 0.1, 0.2, 0.9, -0.5, 0.4, 0.2, 0.3, 0.1, 0.2]
        cos = Cos.from_coefficients(coef, mpol=4, ntor=1, num_field_periods=5)
        np.testing.assert_almost_equal(cos([0, 0, 0]), sum(coef))

    def test_cos(self):
        #  Test cos computation at theta=0.0 and phi=0.0 where output
        #  is by definition sum(coef[s])
        ns = 10
        coef = np.arange(ns * 2 * 3).reshape(ns, 2, 3)  # s=10, m=1, n=1
        series = self.get_series(coef, self.num_field_periods)
        #  Test 10 locations along radial profile
        locs = np.zeros((10, 3))
        locs[:, 0] = np.linspace(0, 1, num=10)
        values = series(locs)
        for i in range(10):
            np.testing.assert_array_equal(values[i], series.coef[i].sum())


class TestSin(FourierTermTests, unittest.TestCase):
    class_name = "Sin"
    num_field_periods = 5
    coef = [[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]], [[0.0, 1.0, -0.2], [-0.5, 0.2, 0.5]]]

    test_locs_values = [
        ([0, 0, 0], 0.0),
        ([0, 0, np.pi / 2 / 5], -0.9),
        ([1, 0, np.pi / 2 / 5], -0.8),
        ([0.005, 0, np.pi / 2 / 5], -0.9 * 0.995 - 0.8 * 0.005),  # linear interpolation
        ([0, np.pi / 2, 0], 0.2),
        ([0, np.pi / 2, np.pi / 2 / 5], 0.3),
        (tfields.Points3D([[0, 0, 0]]), 0.0),
        (tfields.Points3D([[0, 0.0, np.pi / 2 / 5], [0, np.pi / 2, 0]]), [-0.9, 0.2]),
    ]

    @staticmethod
    def get_series(*args, **kwargs):
        return Sin(*args, **kwargs)

    @staticmethod
    def get_series_from_coefficients(*args, **kwargs):
        return Sin.from_coefficients(*args, **kwargs)


class TestFourier(unittest.TestCase):
    class_name = "Fourier"
    num_field_periods = 5
    cos = [[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]]]
    sin = [[[0.0, 0.0, 0.33], [-0.1, 0.1, 0.1]]]

    test_locs_values = [
        ([0, 0, 0], 1.1),
        (tfields.Points3D([[0, 0, 0]]), 1.1),
        ([0, 0, np.pi / 2 / 5], 0.67),
        ([0, np.pi / 2, 0], 1.0),
        ([0, np.pi / 2, np.pi / 2 / 5], 1.77),
    ]

    @staticmethod
    def get_series(*args, **kwargs):
        return Fourier(*args, **kwargs)

    @staticmethod
    def get_series_from_coefficients(*args, **kwargs):
        return Fourier.from_coefficients(*args, **kwargs)

    ####################
    # CLASS ONLY TESTS #
    ####################

    def test_init_sin_none(self):
        series = Fourier(cos=self.cos, num_field_periods=self.num_field_periods)
        self.assertEqual(series.sin, None)

    def test_init_cos_none(self):
        series = Fourier(sin=self.sin, num_field_periods=self.num_field_periods)
        self.assertEqual(series.cos, None)

    def test_init_from_array(self):
        series = Fourier(
            cos=self.cos, sin=self.sin, num_field_periods=self.num_field_periods
        )
        cos = Cos(coef=self.cos, num_field_periods=self.num_field_periods)
        sin = Sin(coef=self.sin, num_field_periods=self.num_field_periods)
        np.testing.assert_array_equal(series.cos.coef, cos.coef)
        np.testing.assert_array_equal(series.sin.coef, sin.coef)
        self.assertEqual(series.num_field_periods, cos.num_field_periods)

    def test_init_same_num_field_periods(self):
        cos = Cos(coef=self.cos, num_field_periods=self.num_field_periods)
        #  This should take the num_field_periods from the cos term
        series = Fourier(cos=cos, sin=self.sin)
        sin = Sin(coef=self.sin, num_field_periods=self.num_field_periods)
        np.testing.assert_array_equal(series.cos.coef, cos.coef)
        np.testing.assert_array_equal(series.sin.coef, sin.coef)
        self.assertEqual(series.num_field_periods, cos.num_field_periods)

    def test_call(self):
        series = self.get_series(self.cos, self.sin, self.num_field_periods)
        for loc, value in self.test_locs_values:
            np.testing.assert_almost_equal(
                series(loc), value, err_msg=str((loc, value))
            )

    def test_init_from_coefficients(self):
        #  Use VMEC output too
        series = Fourier.from_coefficients(
            cos=list(np.array(self.cos).flatten())[1:],
            sin=list(np.array(self.sin).flatten())[1:],
            num_field_periods=self.num_field_periods,
        )
        cos = Cos(coef=self.cos, num_field_periods=self.num_field_periods)
        sin = Sin(coef=self.sin, num_field_periods=self.num_field_periods)
        np.testing.assert_array_equal(series.cos.coef, cos.coef)
        np.testing.assert_array_equal(series.sin.coef, sin.coef)
        self.assertEqual(series.num_field_periods, cos.num_field_periods)


class TestTensorSeries(unittest.TestCase):
    num_field_periods = 5
    cos = [[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]]]
    sin = [[[0.0, 0.0, 0.33], [-0.1, 0.1, 0.1]]]
    mpol = 1
    ntor = 1

    test_locs_values = [
        ([0, 0, 0], [1.1, 0.0]),
        (tfields.Points3D([[0, 0, 0]]), [1.1, 0.0]),
    ]

    def get_tensor_series_from_coefficients(self):
        return TensorSeries(
            Fourier.from_coefficients(
                cos=self.cos,
                mpol=self.mpol,
                ntor=self.ntor,
                num_field_periods=self.num_field_periods,
            ),
            Fourier.from_coefficients(
                sin=self.sin,
                mpol=self.mpol,
                ntor=self.ntor,
                num_field_periods=self.num_field_periods,
            ),
        )

    def get_tensor_series_from_fourier(self):
        return TensorSeries(
            Fourier(cos=Cos(self.cos, num_field_periods=self.num_field_periods)),
            Fourier(sin=Sin(self.sin, num_field_periods=self.num_field_periods)),
        )

    def test_empty(self):
        tensor = TensorSeries()
        self.assertListEqual(tensor.dims, [])

    def test_from_coefficients_not_empty(self):
        tensor = self.get_tensor_series_from_coefficients()
        self.assertEqual(tensor.ndim, 2)

    def test_from_fourier_not_empty(self):
        tensor = self.get_tensor_series_from_fourier()
        self.assertEqual(tensor.ndim, 2)

    def test_from_coefficients_equal_coef(self):
        from_coefficients = self.get_tensor_series_from_coefficients()
        from_fourier = self.get_tensor_series_from_fourier()
        np.testing.assert_array_equal(
            from_coefficients.dims[0].cos.coef, from_fourier.dims[0].cos.coef
        )
        np.testing.assert_array_equal(
            from_coefficients.dims[1].sin.coef, from_fourier.dims[1].sin.coef
        )

    def test_from_coefficients_call(self):
        tensor = self.get_tensor_series_from_coefficients()
        for loc, value in self.test_locs_values:
            np.testing.assert_almost_equal(
                tensor(loc), value, err_msg=str((loc, value))
            )


class TestInterp(unittest.TestCase):
    num = 101
    xp = np.linspace(0, num - 1, num=num)
    fp = np.random.random(num * 2 * 2).reshape((num, 2, 2))

    def check_results(x, xp, fp):
        np_interp = np.interp(x, xp, fp)
        our_interp = interp(x, xp, fp)
        np.testing.assert_almost_equal(our_interp, np_interp)

    def test_scalar(self):
        #  Scale value to match xp range
        values = np.random.random(10) * (self.num - 1)
        TestInterp.check_results(values, self.xp, self.fp[:, 0, 0])

    def test_boundaries(self):
        values = [0, self.num - 1]
        TestInterp.check_results(values, self.xp, self.fp[:, 0, 0])

    def test_domain(self):
        TestInterp.check_results(self.xp, self.xp, self.fp[:, 0, 0])

    def test_nd(self):
        values = np.random.random(10) * (self.num - 1)
        our_interp = interp(values, self.xp, self.fp)
        np_interp = np.array(
            [
                np.interp(values, self.xp, self.fp.reshape((self.num, -1))[:, i])
                for i in range(np.prod(self.fp.shape[1:]))
            ]
        ).T.reshape((-1, *self.fp.shape[1:]))
        np.testing.assert_almost_equal(our_interp, np_interp)
