import pytest
import importlib.metadata
from datetime import datetime
from unittest import mock
from tempfile import NamedTemporaryFile
import numpy as np

import tfields
from w7x.core import CallHistory
from w7x.simulation.vmec import Vmec
from w7x.simulation.flt import FieldLineTracer


def test_autoset_history_by_call():
    flt = FieldLineTracer(strategy="mock", mode="find_lcfs")
    state = flt()
    state.history.calls[0]


def test_consecutive():
    flt = FieldLineTracer(strategy="mock", mode="find_lcfs")
    state = flt(
        flt.trace_surface(n_points=42, start_point=tfields.Points3D([[6.2, 0.0, 0.0]]))
    )
    assert len(state.history.calls) == 2


@pytest.mark.parametrize(
    "function,module_name",
    [
        (Vmec(strategy="mock").free_boundary, "w7x"),
        (FieldLineTracer(strategy="mock", mode="find_lcfs"), "w7x"),
        (np.array, "numpy"),
        (lambda: 1, "tests"),
    ],
)
def test_CallHistory_init(function, module_name):
    parameters = {"param1": 1, "param2": 2}
    call_time = datetime.now()
    if module_name == "tests":
        version = None
    else:
        version = importlib.metadata.version(module_name)

    with mock.patch("w7x.core.git") as mock_git:
        mock_git.Repo.return_value.head.commit.hexsha = "abc123"

        history = CallHistory(
            parameters=parameters, function=function, call_time=call_time
        )

        assert history.parameters == parameters
        assert isinstance(history.function, str)
        assert history.call_time == call_time
        assert history.module == module_name
        assert history.version == version
        assert history.git_hash == "abc123"

        # Save the CallHistory object to a pickle file

        with NamedTemporaryFile(suffix=".pickle") as tmpfile:
            history.save(tmpfile.name)

            # Load the CallHistory object from the pickle file
            tmpfile.seek(0)
            loaded_history = CallHistory.load(tmpfile.name)

        # Test the fields of the loaded CallHistory object
        assert loaded_history.parameters == parameters
        assert isinstance(history.function, str)
        assert loaded_history.call_time == call_time
        assert loaded_history.module == module_name
        assert loaded_history.version == version
        assert loaded_history.git_hash == "abc123"
