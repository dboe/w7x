import tfields
import w7x
from tempfile import NamedTemporaryFile


def test_save_load_dat():
    dat_path = w7x.config.extender.test.example_dat
    tensors = tfields.Tensors.load(dat_path, max_rows=3)  # 3 rows just for speed
    res = tfields.Tensors(
        [
            [-0.01832099, 0.01834125, -0.00045719],
            [-0.01831959, 0.01852238, 0.00011127],
            [-0.01829761, 0.01869181, 0.00069312],
        ]
    )
    assert (tensors == res).all()

    out_file = NamedTemporaryFile(suffix=".dat")
    tensors.save(out_file.name)
    _ = out_file.seek(0)  # this is only necessary in the test
    tensors_load = tfields.Tensors.load(out_file.name)
    assert tensors.equal(tensors_load)


def test_save_load_kisslinger():
    kisslinger_path = w7x.config.components.test.example_kisslinger
    mesh = tfields.Mesh3D.load(kisslinger_path, rescale_poloidal_resolution=20)
    assert mesh.name == "div_hor_upper_sheet"
    assert mesh.coord_sys == tfields.bases.CYLINDER
    # unit = m
    assert (mesh[:, 0] < 7).all()
    assert (mesh[:, 0] > 4).all()
