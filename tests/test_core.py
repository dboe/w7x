#!/usr/bin/env python  # pylint: disable=protected-access,missing-function-docstring,missing-class-docstring
"""Tests for `w7x` package."""

import abc
import unittest
from dask.delayed import Delayed
import w7x
import pytest


SPECIAL_NUMERIC_PARAMETER = 137
START_POINTS = 12500


# pylint:disable=too-few-public-methods
class ExampleFlavour(abc.ABC):
    @abc.abstractmethod
    @w7x.dependencies(
        w7x.config.CoilSets.Ideal,
        start_points=START_POINTS,
    )
    def required_method_with_dependencies(self, state, **kwargs):
        """
        This is an abstract method. You can use the dependencies with the dependencies.extend
        classmethod of the decorator.
        """


class DummyAttr(w7x.core.StateLeaf):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        super().__init__()


class Example(ExampleFlavour):
    @w7x.node
    @w7x.dependencies(
        w7x.config.CoilSets.Ideal,
        w7x.config.Assemblies.Pfcs,
        start_points=START_POINTS,
    )
    # pylint:disable=unused-argument,no-self-use
    def method(self, state, **kwargs):
        """
        Dummy method
        """
        return DummyAttr(1, z=3, **kwargs)

    @w7x.node
    @w7x.dependencies.extends(
        ExampleFlavour.required_method_with_dependencies,
        w7x.config.Assemblies.Pfcs,
        special_numeric_parameter=SPECIAL_NUMERIC_PARAMETER,
    )
    # pylint:disable=unused-argument,no-self-use
    def required_method_with_dependencies(self, state, **kwargs):
        """
        Concrete implementation of a required method
        """
        return DummyAttr(1, z=3, **kwargs)

    @w7x.node
    @w7x.dependencies.extends(
        ExampleFlavour.required_method_with_dependencies,
        default_attributes=None,
        start_points=w7x.dependencies.REJECTED,
    )
    def method_with_rejected_parameter(self, state, **kwargs):
        """
        Method with a rejected parameter
        """
        return DummyAttr(1, z=3, **kwargs)


class TestNode(unittest.TestCase):
    def setUp(self):
        self.obj = Example()

    def test_distributed_switch(self):
        with w7x.distribute():
            graph = self.obj.method()
            self.assertIsInstance(graph, Delayed)
            res = graph.compute()
        self.assertNotIsInstance(res, Delayed)
        with w7x.distribute(False):
            res = self.obj.method()
        self.assertIsInstance(res, w7x.State)


class TestDependencies(unittest.TestCase):
    def setUp(self):
        self.obj = Example()
        self.functions = [
            self.obj.method,
            self.obj.required_method_with_dependencies,
        ]

    def check_not_stateful(self, state):
        self.assertNotIn("coil_set", state.keys())
        self.assertNotIn("assembly", state.keys())
        self.assertIn("dummy_attr", state.keys())

    # pylint:disable=no-value-for-parameter
    def test_vanilla(self):
        for fun in self.functions:
            with w7x.distribute(False), w7x.stateful(False):
                state = w7x.State(fun())
            self.check_not_stateful(state)

    # pylint:disable=no-value-for-parameter,no-member
    def test_stateful(self):
        """
        Testing the two equivalent decorators with method
        """
        for fun in self.functions:
            with w7x.distribute(False), w7x.stateful(True):
                state = fun()
            self.assertIn("coil_set", state.keys())
            self.assertIn("assembly", state.keys())
            self.assertIn("dummy_attr", state.keys())
            self.assertTrue(state.has(DummyAttr))
            self.assertListEqual(
                [g.name for g in state.coil_set.coils],
                [
                    "ideal non planar coils",
                    "ideal planar coils",
                    "ideal island control coils",
                    "ideal trim coils",
                ],
            )
            self.assertListEqual(
                [g.name for g in state.assembly.components],
                [
                    "divertor",
                    "baffle",
                    "tda",
                    "heat shield",
                    "panel",
                    "vessel",
                    "mesh end",
                ],
            )

    def test_distributed(self):
        for fun in self.functions:
            with w7x.distribute(True), w7x.stateful(False):
                self.assertTrue(w7x.distribute.enabled())
                self.assertFalse(w7x.stateful.enabled())
                graph = w7x.State.merged(fun())
                state = graph.compute()
            self.check_not_stateful(state)

    def test_distributed_arguments(self):
        assemblies = w7x.config.Assemblies.PfcsOp11()
        for fun in self.functions:
            with w7x.distribute(True), w7x.stateful(True):
                graph = fun(assemblies)
                # import pdb; pdb.set_trace()
                state = graph.compute()
            self.assertListEqual(
                [g.name for g in state.assembly.components],
                ["limiter(OP1.1)", "vessel"],
            )

    def test_stateful_arguments(self):
        assemblies = w7x.config.Assemblies.PfcsOp11()
        for fun in self.functions:
            with w7x.distribute(False), w7x.stateful(True):
                state = fun(assemblies)
            self.assertListEqual(
                [g.name for g in state.assembly.components],
                ["limiter(OP1.1)", "vessel"],
            )

    def test_default_state(self):
        for fun in self.functions:
            # Testing retrieval of method defaults
            default_state = w7x.dependencies.default_state(fun)
            self.assertTrue(default_state.has(w7x.config.CoilSets.Ideal))
            self.assertTrue(default_state.has(w7x.config.Assemblies.Pfcs))

    def test_default_parameters(self):
        for fun in self.functions:
            parameters = dict(start_points=START_POINTS)
            if fun.__name__ == self.obj.required_method_with_dependencies.__name__:
                parameters["special_numeric_parameter"] = SPECIAL_NUMERIC_PARAMETER
            self.assertDictEqual(w7x.dependencies.default_parameters(fun), parameters)

    def test_rejected_default_parameter_raises_error(self):
        # does not raise
        self.obj.method_with_rejected_parameter()
        # raises
        with pytest.raises(AttributeError, match="Parameter 'start_points' rejected"):
            self.obj.method_with_rejected_parameter(start_points=3)
