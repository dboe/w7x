import numpy as np
import tfields
import w7x


def test_mgrid():
    base_vectors, iter_order, nfp = w7x.config.get_mgrid_options()
    grid = tfields.TensorGrid.empty(
        *base_vectors, coord_sys=tfields.bases.CYLINDER, iter_order=iter_order
    )

    points = tfields.TensorGrid.empty(
        (4.05, 6.75, 181j),  # w7as had half the resolution
        (0, 2 * np.pi / 5, 96j),
        (-1.35, 1.35, 181j),  # w7as had half the resolution
        coord_sys=tfields.bases.CYLINDER,
        iter_order=[1, 0, 2],  # phi, r, z
    )
    assert nfp == 5
    assert grid.coord_sys == points.coord_sys
    assert grid.base_vectors.equal(points.base_vectors)
    assert (grid.iter_order == points.iter_order).all()
    assert (grid.num == points.num).all()
