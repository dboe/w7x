import pytest
import dataclasses
import dask
from dask.delayed import Delayed
import w7x


@dask.delayed
def optimize_coils(state) -> w7x.model.CoilSet:
    return w7x.model.CoilSet(coils=[w7x.model.Coil(id=-1, name="mock")])


@dask.delayed
def estimate_plasma_parameters(state) -> w7x.model.PlasmaParameters:
    return w7x.config.Plasma.Vacuum()


@dask.delayed
@w7x.dependencies(w7x.model.PlasmaParameters(), w7x.config.CoilSets.Deformed)
def compute_equilibrium(state):
    return w7x.model.Equilibrium(plasma_volume=2)


@dask.delayed
def extend_equilibrium(state):
    equi = w7x.model.Equilibrium(plasma_volume=2 * state.equilibrium.plasma_volume)
    return equi


@dask.delayed
@w7x.dependencies(id_=3)
def build_machine(state, **kwargs):
    return w7x.model.Assembly(components=[w7x.model.Component(id=kwargs.pop("id_"))])


@dask.delayed
@w7x.dependencies(
    w7x.config.CoilSets.Ideal,
    w7x.model.Assembly(components=[w7x.model.Component(id=156)]),
)
def compute_heat_load(state):
    @dataclasses.dataclass
    class HeatLoad(w7x.StateLeaf):
        load: float = None

    return HeatLoad(
        load=state.equilibrium.plasma_volume * state.assembly.components[0].id
    )


@pytest.mark.parametrize("distribute", [True, False])
def test_graph_workflow(distribute):
    state = w7x.State(w7x.model.Assembly(components=[w7x.model.Component(id=3)]))
    with w7x.distribute(True):
        graph = w7x.State.merged(
            state,
            compute_heat_load(
                w7x.State.merged(
                    extend_equilibrium(
                        compute_equilibrium(
                            w7x.State.merged(
                                state,
                                optimize_coils(state),
                                estimate_plasma_parameters(state),
                            )
                        )
                    ),
                    build_machine(state),
                )
            ),
        )
        # graph.visualize(
        #     filename=w7x.config.general.tmp_dir
        #     + "/graph_workflow_test_visualization.pdf"
        # )
        assert isinstance(graph, Delayed)
        state = w7x.compute(graph)
    assert state.equilibrium.plasma_volume == 4
    assert state.heat_load.load == 12
