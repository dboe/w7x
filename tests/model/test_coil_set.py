import pytest
import numpy as np
import w7x
import w7x.model
import w7x.config


options_flat = [True, False]
options_independent = [True, False]
options_units = ["rw", "Aw", "A", "r"]
options_currents_A = [2, 20.0]

reference_coil = w7x.config.CoilTypes.NonPlanar(current=13200)
n_windings = 36
reference_n_windings = reference_coil.n_windings
reference_current = reference_coil.current
currents_dict = {
    current: {
        "A": current,
        "Aw": (current * n_windings),
        "r": current / reference_current,
        "rw": (current * n_windings) / (reference_current * reference_n_windings),
    }
    for current in options_currents_A
}


class Test_Basic_Coil_Set:
    # unit = request.param
    # high_iota = w7x.config.MagneticConfig.high_iota_rw
    # ideal_hm = w7x.config.CoilSets.Ideal.from_currents(
    #     *high_iota[:7], 8 / 108, 8 / 108, unit="rw"
    # )
    # return ideal_hm
    def get_cs(self):
        cs = w7x.model.CoilSet(
            coils=[
                w7x.model.CoilSet(
                    coils=[
                        w7x.model.Coil(id=0, current=42),
                        w7x.model.Coil(id=1, current=42),
                    ],
                    shared_supply=[(0, 1)],
                ),
                w7x.model.Coil(id=2, current=21),
            ],
        )
        return cs

    currents = [[42, 42], 21]
    currents_flat = [42, 42, 21]
    currents_independent = [[42], 21]
    currents_independent_flat = [42, 21]
    supply_coil_ids = [0, 0, 2]

    @pytest.mark.parametrize("independent", options_independent)
    @pytest.mark.parametrize("flat", options_flat)
    def test_get_currents(self, independent, flat):
        attr = (
            f"currents{'_independent' if independent else ''}{'_flat' if flat else ''}"
        )
        expected = getattr(self, attr, None)
        if expected is None:
            pytest.skip(f"Result {attr} not provided")
        cs = self.get_cs()
        currents = cs.get_currents(unit="A", independent=independent, flat=flat)
        assert currents == expected

    @pytest.mark.parametrize("independent", options_independent)
    @pytest.mark.parametrize("flat", options_flat)
    def test_set_currents(self, independent, flat):
        attr = (
            f"currents{'_independent' if independent else ''}{'_flat' if flat else ''}"
        )
        expected = getattr(self, attr, None)
        if expected is None:
            pytest.skip(f"Result {attr} not provided")
        # reset all coils
        cs = self.get_cs()
        coils = cs.get_coils(flat=True)
        for coil in coils:
            coil.current = -1
        # set now
        cs.set_currents(expected, unit="A", independent=independent, flat=flat)
        currents = cs.get_currents(unit="A", independent=independent, flat=flat)
        assert currents == expected

    def test_supply_coils(self):
        cs = self.get_cs()
        coils = cs.get_coils(flat=True)
        scs = []
        for coil in coils:
            supply = coil.supply_coil()
            scs.append(supply.id)
        assert scs == self.supply_coil_ids

    def test_from_currents(self):
        cs = self.get_cs()
        cs = cs.from_currents(
            *self.currents_independent_flat[:1], coils=cs.coils.copy(), unit="A"
        )
        # all fcurrents are 0 after the first
        currents = cs.get_currents(flat=True, independent=True)
        assert currents[0] == self.currents_independent_flat[0]
        if len(currents) > 1:
            assert set(currents[1:]) == {0.0}

    def test_equals(self):
        # equals self
        cs = self.get_cs()
        assert cs.equals(cs)
        other = self.get_cs()
        assert cs.equals(other)
        other.remove(other._children[0])
        assert not cs.equals(other)
        third = self.get_cs()
        third.set_currents([c**2 - 1 for c in cs.get_currents(flat=True)])
        assert not cs.equals(third)
        assert cs.equals(third, compare_current=False)


class Test_Single_Nested_Coil_Set(Test_Basic_Coil_Set):
    # unit = request.param
    # high_iota = w7x.config.MagneticConfig.high_iota_rw
    # ideal_hm = w7x.config.CoilSets.Ideal.from_currents(
    #     *high_iota[:7], 8 / 108, 8 / 108, unit="rw"
    # )
    # return ideal_hm
    def get_cs(self):
        cs = w7x.model.CoilSet(
            coils=[
                w7x.model.CoilSet(
                    coils=[
                        w7x.model.Coil(id=0, current=42),
                        w7x.model.Coil(id=1, current=21),
                        w7x.model.Coil(id=10, current=21),
                        w7x.model.Coil(id=2, current=10.5),
                        w7x.model.Coil(id=3, current=5.25),
                        w7x.model.Coil(id=4, current=2.625),
                        w7x.model.Coil(id=11, current=21),
                        w7x.model.Coil(id=5, current=1.3125),
                    ],
                    shared_supply=[(1, 2, 6)],
                ),
                w7x.model.Coil(id=6, current=0.65625),
            ],
        )
        return cs

    currents = [[42, 21, 21, 10.5, 5.25, 2.625, 21, 1.3125], 0.65625]
    currents_flat = [42, 21, 21, 10.5, 5.25, 2.625, 21, 1.3125, 0.65625]
    currents_independent = [[42, 21, 10.5, 5.25, 2.625, 1.3125], 0.65625]
    currents_independent_flat = [42, 21, 10.5, 5.25, 2.625, 1.3125, 0.65625]
    supply_coil_ids = [0, 1, 1, 2, 3, 4, 1, 5, 6]


class Test_Double_Nested_Coil_Set(Test_Basic_Coil_Set):
    # unit = request.param
    # high_iota = w7x.config.MagneticConfig.high_iota_rw
    # ideal_hm = w7x.config.CoilSets.Ideal.from_currents(
    #     *high_iota[:7], 8 / 108, 8 / 108, unit="rw"
    # )
    # return ideal_hm
    def get_cs(self):
        cs = w7x.model.CoilSet(
            coils=[
                w7x.model.CoilSet(
                    coils=[
                        w7x.model.CoilSet(
                            coils=[
                                w7x.model.Coil(id=0, current=42),
                                w7x.model.Coil(id=1, current=42),
                            ],
                            shared_supply=[(0, 1)],
                        ),
                        w7x.model.CoilSet(
                            coils=[
                                w7x.model.Coil(id=3, current=21),
                                w7x.model.Coil(id=4, current=21),
                            ],
                            shared_supply=[(0, 1)],
                        ),
                    ],
                ),
                w7x.model.CoilSet(
                    coils=[
                        w7x.model.CoilSet(
                            coils=[
                                w7x.model.Coil(id=5, current=10.5),
                                w7x.model.Coil(id=6, current=10.5),
                            ],
                            shared_supply=[(0, 1)],
                        ),
                    ],
                ),
            ],
        )
        return cs

    currents = [[[42, 42], [21, 21]], [[10.5, 10.5]]]
    currents_flat = [42, 42, 21, 21, 10.5, 10.5]
    currents_independent = [[[42], [21]], [[10.5]]]
    currents_independent_flat = [42, 21, 10.5]
    supply_coil_ids = [0, 0, 3, 3, 5, 5]


class Test_CoilSet:
    def get_cs(self):
        return w7x.config.CoilSets.Ideal()

    @pytest.mark.parametrize("unit", options_units)
    @pytest.mark.parametrize("check_unit", options_units)
    def test_current_units(self, unit, check_unit):
        cs = self.get_cs()
        currents = cs.coil_currents(unit=unit)
        m_tmp = w7x.config.CoilSets.Ideal.from_currents(*currents, unit=unit)
        np.testing.assert_almost_equal(
            m_tmp.coil_currents(check_unit),
            cs.coil_currents(check_unit),
        )


class Test_MagneticConfig_FromCurrents(Test_CoilSet):
    def get_cs(self):
        return w7x.config.CoilSets.Ideal.from_currents(
            1,
            1,
            1,
            1,
            1,
            -0.23,
            -0.23,
            0.001,
            0.001,
            0.0002,
            0.0002,
            0.0002,
            0.0002,
            0.0002,
            unit="rw",
        )
