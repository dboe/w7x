import pytest
import w7x
import w7x.model
import w7x.config


options_flat = [True, False]
options_independent = [True, False]
options_units = ["rw", "Aw", "A", "r"]
options_currents_A = [2, 20.0]

reference_coil = w7x.config.CoilTypes.NonPlanar(current=13200)
n_windings = 36
reference_n_windings = reference_coil.n_windings
reference_current = reference_coil.current
currents_dict = {
    current: {
        "A": current,
        "Aw": (current * n_windings),
        "r": current / reference_current,
        "rw": (current * n_windings) / (reference_current * reference_n_windings),
    }
    for current in options_currents_A
}


@pytest.mark.parametrize("current", currents_dict.keys())
@pytest.mark.parametrize("unit_from", options_units)
@pytest.mark.parametrize("unit_to", options_units)
def test_convert_current(current, unit_from, unit_to):
    ref_dict = {}
    if "r" in unit_from or "r" in unit_to:
        ref_dict = dict(
            reference_n_windings=reference_n_windings,
            reference_current=reference_current,
        )
    converted = w7x.model.convert_current(
        currents_dict[current][unit_from],
        unit_from,
        unit_to=unit_to,
        n_windings=n_windings,
        **ref_dict
    )
    assert currents_dict[current][unit_to] == converted
