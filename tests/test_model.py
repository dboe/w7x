# pylint:disable=missing-function-docstring,missing-module-docstring,missing-class-docstring,invalid-name,protected-access
import unittest
import numpy as np

import tfields
import w7x
import w7x.simulation.flt
import w7x.lib.dataclasses as dataclasses


class ComponentTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_entry(self):
        @dataclasses.dataclass
        class DummyAttr(w7x.StateLeaf):
            val: int = 4

        @w7x.node
        @w7x.dependencies(w7x.model.Assembly(components=[w7x.model.Component(id=3)]))
        def dummy_state(state, **kwargs):
            return DummyAttr(val=7)

        graph = dummy_state()  # noqa
        state = graph.compute()
        self.assertTrue(state.has(DummyAttr))
        self.assertTrue(state.has(w7x.model.Assembly))

    def check_state(self, state, other):
        # same length
        self.assertEqual(
            len(state.assembly.get_components(flat=True)),
            len(other.assembly.get_components(flat=True)),
        )
        # same position
        for comp_a, comp_b in zip(
            state.assembly.get_components(flat=True),
            other.assembly.get_components(flat=True),
        ):
            self.assertEqual(comp_a.id, comp_b.id)
            if comp_a.id in [
                c.id for c in w7x.config.AssemblyGroups.Divertor().components
            ]:
                # merged fields
                for field in dataclasses.fields(comp_a):
                    if field.name not in ["mesh", "slices"]:
                        continue
                    val_a = getattr(comp_a, field.name)
                    val_b = getattr(comp_b, field.name)
                    self.assertIsNone(val_a)
                    self.assertIsNotNone(val_b)

    def test_merged_same_structure(self):
        state = w7x.State(w7x.config.Assemblies.Pfcs())
        pfcs = w7x.config.Assemblies.Pfcs()
        for group in pfcs.components:
            if isinstance(group, w7x.config.AssemblyGroups.Divertor):
                for comp in group.components:
                    comp.mesh = tfields.Mesh3D([])
                    comp.slices = {0: tfields.TensorMaps([[1, 2, 3], [4, 5, 6]])}
        state_merge = w7x.State.merged(state, pfcs)
        self.check_state(state, state_merge)

    def test_merged_subset(self):
        state = w7x.State(w7x.config.Assemblies.Pfcs())
        divertor = w7x.model.Assembly()
        divertor.add(w7x.config.AssemblyGroups.Divertor())
        for comp in divertor.get_components(flat=True):
            comp.mesh = tfields.Mesh3D([])
            comp.slices = {0: tfields.TensorMaps([[1, 2, 3], [4, 5, 6]])}

        state_merge = w7x.State.merged(state, divertor)
        self.check_state(state, state_merge)

    def test_merged_flat_subset(self):
        state = w7x.State(w7x.config.Assemblies.Pfcs())
        divertor = w7x.config.AssemblyGroups.Divertor()
        for comp in divertor.components:
            comp.mesh = tfields.Mesh3D([])
            comp.slices = {0: tfields.TensorMaps([[1, 2, 3], [4, 5, 6]])}
        state_merge = w7x.State.merged(state, divertor)
        self.check_state(state, state_merge)


class AssemblyTest(unittest.TestCase):
    def setUp(self):
        self.obj = w7x.config.Assemblies.PfcsOp11()

    def test_parent(self):
        div = w7x.config.AssemblyGroups.Divertor()
        self.assertIs(div.components[0].parent, div)


class FileTest(unittest.TestCase):
    def test_mgrid_dat(self):
        path = w7x.config.extender.test.example_dat
        base_vectors = ((3.81, 6.87, 173j), (0, 2 * np.pi, 280j), (-1.45, 1.45, 170j))
        mgrid = w7x.model.MGrid(
            path=path,
            base_vectors=base_vectors,
            iter_order=[1, 0, 2],
        )
        state = mgrid.to_state()
        grid_file = state.equilibrium.get_ref("field")
        self.assertIs(grid_file, mgrid)
        self.assertTupleEqual(
            grid_file.base_vectors,
            ((3.81, 6.87, 173j), (0, 2 * np.pi, 280j), (-1.45, 1.45, 170j)),
        )
        self.assertTrue(grid_file.path.endswith(".dat"))

        # so far the example file has not been read.
        self.assertIsNone(grid_file.data)

        # The .dat format is a stupid one which is however wide spread. You can save the full
        # information with the tfields.TensorGrid type. The above field was saved with the below
        # commands:
        # field = state.equilibrium.field
        # # loading data has been triggered
        # self.assertIsNotNone(grid_file.data)
        # field.save(w7x.config.extender.test.example_npz

    def test_mgrid_tensor_grid(self):
        path_npz = w7x.config.extender.test.example_npz
        mgrid = w7x.model.MGrid(path=path_npz)
        state = mgrid.to_state()
        grid_file = state.equilibrium.get_ref("field")
        self.assertIs(grid_file, mgrid)
        # so far the example file has not been read.
        self.assertIsNone(grid_file.data)
        self.assertTupleEqual(
            grid_file.base_vectors,
            ((3.81, 6.87, 173j), (0, 2 * np.pi, 280j), (-1.45, 1.45, 170j)),
        )
        # loading the base_vectors required loading the data
        self.assertIsNotNone(grid_file.data)
        self.assertTrue(grid_file.path.endswith(".npz"))
        field = state.equilibrium.field
        self.assertTrue(field.is_empty())
