"""
Tests of extender front- and backends
"""
import pytest
import unittest
import numpy as np

import rna
import tfields
import w7x
import w7x.simulation.extender
import w7x.simulation.vmec


class ExtenderTest(unittest.TestCase):
    """
    Unittest for Extender
    """

    VMEC_ID = "dboe_id_1000_1000_1000_1000_+0000_+0000_v_00_pres_00_it_12"
    # VMEC_ID = "w7x_v_0.1.0.dev6_id_1000_1000_1000_1000_+0000_+0000_pres_00_it_0"

    def setUp(self):
        stdcase_vacuum_wout = rna.path.resolve(
            w7x.__file__,
            "../../tests/resources/vmec",
            "wout_" + self.VMEC_ID + ".nc",
        )

        self.wout = w7x.simulation.vmec.Wout(
            path=stdcase_vacuum_wout,
            file_id=self.VMEC_ID,
        )
        self.resources = w7x.model.Resources()
        self.resources.add(self.wout)
        self.equi = w7x.model.Equilibrium()
        self.points = tfields.Points3D(
            np.transpose(
                [
                    [5.5, 5.6, 5.6, 5.6, 5.6, 5.6, 4.6, 4.6, 4.6, 4.6, 4.6],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.628, 0.628, 0.628, 0.628, 0.628],
                    [0.0, -0.2, -0.1, 0.0, 0.1, 0.2, -0.2, -0.1, 0.0, 0.1, 0.2],
                ]
            ),
            coord_sys=tfields.bases.CYLINDER,
        )
        # TODO-2(@dboe): coil_set from vemc_id?
        self.coil_set = w7x.model.CoilSet(
            coils=[
                w7x.config.CoilGroups.IdealNonPlanar(),
                w7x.config.CoilGroups.IdealPlanar(),
            ]
        )

    def check_equilibrium_state(self, state, points=None):
        self.assertTrue(state.has(w7x.model.Equilibrium))
        field = state.equilibrium.field
        if points is not None:
            self.assertTrue(field.equal(tfields.Tensors(points)))
        self.assertTrue(len(field.fields), 1)
        self.assertEqual(field.fields[0].coord_sys, tfields.bases.PHYSICAL_CYLINDER)

    @pytest.mark.slow
    @pytest.mark.web_service
    def test_tensor_grid_generation(self):
        """
        tfields.TensorGrid is default dependencies parameter for points
        """
        state = self.wout.to_state()
        node = w7x.simulation.extender.Extender()
        grid = w7x.config.get_mgrid_options(return_type=tfields.TensorGrid)
        grid = tfields.TensorGrid(grid, num=(3, 3, 3))
        state = node.field(state, points=grid)
        self.check_equilibrium_state(state, points=grid)

    def test_equi(self):
        """
        Test equilibrium type
        """
        self.assertIsInstance(self.equi, w7x.model.Equilibrium)

    @pytest.mark.slow
    @pytest.mark.web_service
    def test_field(self):
        """
        Test the resulting field
        """
        state = w7x.State(self.equi, self.coil_set, self.resources)
        node = w7x.simulation.extender.Extender()

        state = node.field(state, points=self.points)
        self.check_equilibrium_state(state, points=self.points)

    @pytest.mark.slow
    @pytest.mark.web_service
    def test_plasma_field(self):
        """
        Test the plasma field generation
        """
        state = w7x.State(self.equi, self.coil_set, self.resources)
        node = w7x.simulation.extender.Extender()
        with w7x.stateful(False):
            equilibrium = node.plasma_field(state, points=self.points)
        plasma_field = equilibrium.plasma_field
        points_mod_2 = self.points.copy()
        points_mod_2.transform(tfields.bases.CYLINDER)
        points_mod_2[:, 1] += 1 / 5 * (2 * np.pi)
        state_mod_2 = node.plasma_field(state, points=points_mod_2)
        plasma_field_mod_2 = state_mod_2.equilibrium.plasma_field
        self.assertTrue(points_mod_2.equal(plasma_field_mod_2))
        self.assertTrue(
            plasma_field.fields[0].equal(plasma_field_mod_2.fields[0], atol=1e-5)
        )
