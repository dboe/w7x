import unittest

import tfields
import w7x
from w7x.simulation.components import Components


class TestDownload(unittest.TestCase):
    def setUp(self):
        assembly = w7x.config.Assemblies.Pfcs()
        self.state_init = w7x.State(assembly)

    def test_update(self):
        compcode = Components()  # TODO-1(@dboe): confusin name of code!

        mesh_comp = w7x.config.AssemblyGroups.Divertor().components[
            0
        ]  # divertor module 1
        state = compcode.set_meshes(
            self.state_init,
            component=mesh_comp,
        )
        self.assertTrue(
            all(
                [
                    comp.mesh is None
                    for comp in self.state_init.assembly.get_components(flat=True)
                ]
            )
        )
        for comp in state.assembly.get_components(flat=True):
            if comp == mesh_comp:
                self.assertIsInstance(comp.mesh, tfields.Mesh3D)
            else:
                self.assertIsNone(comp.mesh)


class TestSlice(unittest.TestCase):
    def setUp(self):
        assembly = w7x.config.Assemblies.Pfcs()
        self.state_init = w7x.State(assembly)
        self.phi = 0.0
        self.assembly = w7x.config.AssemblyGroups.Divertor()

    def check_state(self, state):
        self.assertTrue(state.has(w7x.model.Assembly))
        self.assertEqual(
            len(state.assembly.get_components(flat=True)),
            len(self.assembly.get_components(flat=True)),
        )
        self.assertTrue(
            all(
                [c.slices is not None for c in state.assembly.get_components(flat=True)]
            )
        )
        self.assertTrue(
            all([len(c.slices) == 1 for c in state.assembly.get_components(flat=True)])
        )
        for c in state.assembly.get_components(flat=True):
            if c.id == 165:
                self.assertIsInstance(c.slices[self.phi], tfields.TensorMaps)
                self.assertTrue(2 in c.slices[self.phi].maps)
                self.assertEqual(len(c.slices[self.phi].maps[2]), 224)
            else:
                self.assertIsNone(c.slices[self.phi])

    def test_slice(self):
        compcode = Components()
        state = compcode.mesh_slice(self.assembly, phi=self.phi)
        self.check_state(state)

    def test_parallel(self):
        with w7x.distribute(True):
            compcode = Components()
            graph = compcode.mesh_slice(self.assembly, phi=self.phi)
            # graph.visualize(filename="componentsGraphDebug.pdf")
            state = graph.compute()
        self.check_state(state)


class TestUpload(unittest.TestCase):
    def setUp(self):
        example_stp = tfields.Mesh3D.load(w7x.config.components.test.example_stp)
        example_stp /= 1e3  # convert to meter
        own_component = w7x.model.Component(
            name="wolfram tile 232044",
            info="Test component",
            mesh=example_stp,
        )
        self.component = own_component

    def test_upload_and_delete_component(self):
        compcode = Components("web_service")  # TODO-1(@dboe): confusin name of code!
        self.assertIsNone(self.component.id)
        component = compcode.backend.upload_component(self.component)
        self.assertIsNotNone(component.id)
        compcode.backend.delete_component(self.component)


class TestBasics(unittest.TestCase):
    def test_filter(self):
        compcode = Components("web_service")
        self.assertEqual(compcode.backend._filter(1), [1])

    def test_exists_id(self):
        compcode = Components("web_service")
        self.assertFalse(compcode.backend.exists(-1))
        self.assertTrue(compcode.backend.exists(1))
