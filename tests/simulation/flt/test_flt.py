# pylint:disable=missing-function-docstring,missing-module-docstring,missing-class-docstring,invalid-name,protected-access
"""
Field line tracer tests
"""
import pytest
import unittest

import tfields
import w7x
import w7x.simulation.flt
import w7x.simulation.backends.web_service
from .test_basic_mock import TestMockedWebServiceTrace

# TODO: skip this test with pytest if the web_service server can not be reached.


SERVER = w7x.simulation.backends.web_service.get_server(
    w7x.config.flt.web_service.server
)


@pytest.mark.skipif(SERVER is None, reason="web_service server not available")
class TestWebService(unittest.TestCase):
    def test_machine_from_state(self):
        example_stp = tfields.Mesh3D.load(w7x.config.components.test.example_stp)
        example_stp /= 1e3  # convert to meters
        own_component = w7x.model.Component(
            name="wolfram tile 232044",
            # info="Info if wished.",
            mesh=example_stp,
        )
        assembly = w7x.model.Assembly()
        assembly.add(w7x.config.AssemblyGroups.Divertor())
        assembly.add(own_component)
        state = w7x.State(assembly)
        from w7x.simulation.backends.web_service.flt import Machine

        inp = Machine.from_state(state).as_input()
        self.assertAlmostEqual(
            inp.meshedModels[0].nodes.x1[0], 4.733342, 6
        )  # points3D type


@pytest.mark.skipif(SERVER is None, reason="web_service server not available")
class TestWebServiceDiffusion(TestMockedWebServiceTrace):
    ADDRESS = w7x.config.flt.web_service.server

    def get_return_value(self):
        """
        Example return from webservice docu
        """
        clr = self.server.types.ConnectionLengthResult()
        clr.length = 17.2488716614
        clr.x = 4.46717707089
        clr.y = -5.04662982128
        clr.z = 0.729193332073
        clr.part = 0
        clr.element = 1245
        element_load = self.server.types.ElementLoad()
        element_load.id = 1245
        element_load.events = 1
        element_load.area = 0.315258312028
        component_load = self.server.types.ComponentLoad()
        component_load.id = 0
        component_load.events = 1
        component_load.elements = [element_load]
        loads = self.server.types.Loads()
        loads.events = 1
        loads.components = [component_load]
        res = self.server.types.Result()
        res.connection = [clr]
        res.loads = loads
        return res

    # pylint:disable=too-many-locals
    def test_trace_diffusion(self):
        flt = w7x.simulation.flt.FieldLineTracer()
        flt.backend = "web_service"
        plasma = w7x.model.PlasmaParameters(diffusion_coeff=1.0, velocity=50000.0)
        start_points = tfields.Points3D([[4.8, 0, 0]])
        coil_set = w7x.model.CoilSet(coils=[w7x.config.CoilGroups.IdealNonPlanar()])
        assembly = w7x.model.Assembly(
            components=[
                w7x.config.AssemblyGroups.MeshEnd(),
                w7x.config.AssemblyGroups.Divertor(),
            ]
        )
        state = flt.trace_diffusion(
            coil_set,
            assembly,
            plasma,
            start_points=start_points,
            step=6.5e-3,
            connection_limit=20000.0,
        )
        self.assertTrue(state.has(w7x.model.Pwi))

        #  From here one copied from web_service trace_connection example
        tracer = self.server

        # tracer.service.buildInfo()

        points = tracer.types.Points3D()
        points.x1 = [4.8]
        points.x2 = [0.0]
        points.x3 = [0.0]

        task = tracer.types.Task()
        task.step = 6.5e-3
        con = tracer.types.ConnectionLength()
        con.limit = 2.0e4
        con.returnLoads = True
        task.connection = con

        diff = tracer.types.LineDiffusion()
        task.diffusion = diff
        diff.diffusionCoeff = 1.0
        diff.freePath = 0.1
        diff.velocity = 5e4

        config = tracer.types.MagneticConfig()
        config.coilsIds = list(range(160, 210))
        config.coilsIdsCurrents = [1425600.0] * 50

        grid = tracer.types.Grid()
        config.grid = grid
        grid.fieldSymmetry = 5  # 1

        cyl = tracer.types.CylindricalGrid()
        grid.cylindrical = cyl
        cyl.numR, cyl.numZ, cyl.numPhi = (
            181,
            181,
            96,
        )  # numphi = 481 if fieldSymmetry = 1
        cyl.RMin, cyl.RMax, cyl.ZMin, cyl.ZMax = 4.05, 6.75, -1.35, 1.35

        machine = tracer.types.Machine(1)
        machine.grid.numX, machine.grid.numY, machine.grid.numZ = 500, 500, 100
        machine.grid.ZMin, machine.grid.ZMax = -1.5, 1.5
        machine.grid.YMin, machine.grid.YMax = -7, 7
        machine.grid.XMin, machine.grid.XMax = -7, 7
        machine.meshedModelsIds = [164, 165, 166, 167, 168, 169]

        config.inverseField = False

        self.server.service.trace.assert_called_with(
            points,
            config,
            task,
            machine,
        )

        # # unmock and run real calculation
        # self.server.service.trace = self.trace
        # res_fwd = tracer.service.trace(points, config, task, machine)


@pytest.mark.skipif(SERVER is None, reason="web_service server not available")
class TestWebServiceDiffusionFail(TestWebServiceDiffusion):
    def get_return_value(self):
        return None
