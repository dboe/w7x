import tfields
import w7x
import w7x.simulation.flt
import unittest
from unittest.mock import MagicMock


BACKEND = "mock"  # 'web_service'  # 'mock'


class TestFullDiffusion(unittest.TestCase):
    def setUp(self):
        self.flt = w7x.simulation.flt.FieldLineTracer()
        self.flt.backend = BACKEND

        state = w7x.State(w7x.config.Assemblies.PfcsOp11())
        graph = self.flt.diffusion(state, start_points=10)
        # graph.visualize(filename="full_diff_graph.pdf")
        self.state = graph.compute()


class TestMockedWebServiceTrace(unittest.TestCase):
    ADDRESS = None

    def setUp(self):
        self.server = w7x.simulation.backends.web_service.get_server(self.ADDRESS)
        self.trace = self.server.service.trace
        mock = MagicMock(return_value=self.get_return_value())

        self.server.service.trace = self.trace
        self.server.service.trace = mock

    def tearDown(self):
        # undo mock
        self.server.service.trace = self.trace


class TestPoincare(unittest.TestCase):
    """
    Test trace_poincare and plotting
    """

    def setUp(self):
        self.flt = w7x.simulation.flt.FieldLineTracer()
        self.flt.backend = BACKEND

        self.n_points = 3
        self.phi_list_rad = [0, 0.05]
        self.seeds = tfields.Points3D([[5.7, 0, 0]])

        state = self.flt.trace_poincare(
            n_points=self.n_points,
            seeds=self.seeds,
            phi=self.phi_list_rad,
        )
        self.state = state.compute()

    def test_poincare(self):
        self.assertEqual(
            len(self.state.traces.poincare_surfaces),
            len(self.seeds),
        )
        for surface in self.state.traces.poincare_surfaces:
            self.assertEqual(len(surface), self.n_points * len(self.phi_list_rad))


class TestDiffusion(unittest.TestCase):
    """
    Test trace_diffusion
    """

    def setUp(self):
        self.flt = w7x.simulation.flt.FieldLineTracer()
        self.flt.backend = BACKEND

        self.start_points = tfields.Points3D([[5.7, 0, 0]] * 3)

        graph = self.flt.trace_diffusion(start_points=self.start_points)

        # graph.visualize(
        #     filename=rna.path.resolve(w7x.config.general.tmp_dir, "diff_graph.pd")
        # )

        self.state = graph.compute()

    def test_require_start_points(self):
        with self.assertRaises(AttributeError) as err:
            self.flt.trace_diffusion()  # pylint:disable=no-value-for-parameter
        self.assertEqual("Parameter 'start_points' required", str(err.exception))
