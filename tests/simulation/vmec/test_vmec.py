"""
VMEC tests.

TODO-2(@amerlo): use object oriented approach.
TODO-2(@dboe): skip tests except mockup on ci
"""

import os
import shutil
import pytest
import unittest
import tempfile
import omegaconf
from datetime import datetime
from time import sleep

import numpy as np

import w7x
from w7x.lib.equilibrium import (
    Fourier,
    TensorSeries,
    Cos,
    Sin,
)
from w7x.lib.profiles import (
    GaussianProcess,
    PowerSeries,
    CubicSpline,
    TwoPower,
    SumAtan,
)
from w7x.simulation.backends.web_service import (
    to_osa_type,
    get_ws_class,
    get_server,
    run_service,
)
from w7x.simulation.vmec import (
    VmecInput,
    Wout,
    Vmec,
    get_run_status_from_threed1,
    Status,
    validate_phi_edge,
    _get_p0_vs_beta,
)
from w7x.simulation.backends.web_service.vmec import VmecWebServiceBackend
from w7x.simulation.backends.runner.vmec import VmecRunnerBackend
from w7x.simulation.backends.mock.vmec import VmecMockupBackend


omegaconf.OmegaConf.set_struct(w7x.config, False)
w7x.config.vmec.mock = omegaconf.OmegaConf.create()
WS_SERVER = w7x.config.vmec.web_service.server


def now():
    """
    Build a string with the present timestamp.
    """
    return datetime.now().strftime("%Y%m%d%H%M%S%f")


def get_default_vmec_input_from_state(**kwargs):
    """
    Method for fast creation of a VMEC input with a proper default.
    """
    # TODO-2(@dboe): Better to use dependency lookup on method
    return VmecInput.from_state(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
        **kwargs,
    )


def get_defaul_vmec_input():
    """
    Build a VMEC input as from web service documentation.

    From: http://webservices.ipp-hgw.mpg.de/docs/vmec.html#VmecInput
    """

    default = get_ws_class(WS_SERVER, "VmecInput")(1)
    default.freeBoundary = True
    default.mgridFile = "mgrid_w7x_nv36_hires.nc"
    default.coilCurrents = [
        1.384814e04,
        1.384814e04,
        1.384814e04,
        1.384814e04,
        1.384814e04,
        8.308887e03,
        8.308887e03,
    ]
    default.intervalFullVacuumCalculation = 6
    default.numFieldPeriods = 5
    default.numModesPoloidal = 12
    default.numModesToroidal = 12
    default.numGridPointsPoloidal = 32
    default.numGridPointsToroidal = 36
    default.numGridPointsRadial = [4, 9, 28, 99]
    default.timeStep = 0.9
    default.tcon0 = 2.0
    default.maxIterationsPerSequence = 6000
    default.intervalConvergenceOutput = 100
    default.forceToleranceLevels = [0.001, 0.00001, 1e-9, 1e-14]
    default.maxToroidalMagneticFlux = -2.64726e00
    default.gamma = 0

    pressure_profile = get_ws_class(WS_SERVER, "Profile")()
    pressure_profile.ProfileType = "power_series"
    pressure_profile.coefficients = [1e-6, -1e-6]
    default.pressureProfile = pressure_profile
    jtor = get_ws_class(WS_SERVER, "Profile")()
    jtor.ProfileType = "power_series"
    jtor.coefficients = [0]
    default.toroidalCurrentProfile = jtor

    default.magneticAxis.RCos.coefficients = [5.54263e00, 1.84047e-1]
    default.magneticAxis.RCos.poloidalModeNumbers = [0]
    default.magneticAxis.RCos.toroidalModeNumbers = [0, 1]
    default.magneticAxis.RCos.numRadialPoints = 1
    default.magneticAxis.ZSin.coefficients = [0.00000, 1.57481e-01]
    default.magneticAxis.ZSin.poloidalModeNumbers = [0]
    default.magneticAxis.ZSin.toroidalModeNumbers = [0, 1]
    default.magneticAxis.ZSin.numRadialPoints = 1

    default.boundary.RCos.coefficients = [
        1.8275e-03,
        1.6341e-03,
        1.2163e-03,
        3.0809e-03,
        5.5289e00,
        2.6718e-01,
        -2.1739e-03,
        -2.8507e-04,
        -1.8275e-03,
        1.6341e-03,
        1.2163e-03,
        3.0809e-03,
        3.4528e-02,
        4.4872e-01,
        -2.7085e-01,
        -4.9284e-03,
        4.9911e-04,
        6.5184e-05,
        4.8734e-04,
        1.4054e-03,
        4.8047e-03,
        1.5134e-02,
        3.5687e-02,
        4.9779e-02,
        6.5200e-02,
        -1.1350e-02,
        -1.6119e-03,
        1.0339e-04,
        -3.2332e-04,
        -3.4468e-04,
        -1.5729e-03,
        -2.0611e-03,
        -1.4756e-02,
        -1.9949e-02,
        -8.5802e-03,
        3.8516e-03,
        1.1352e-04,
        3.6285e-04,
        2.4647e-04,
        6.2828e-04,
        2.7421e-03,
        4.9943e-03,
        7.4223e-03,
        -5.0041e-04,
        -5.9196e-04,
    ]
    default.boundary.RCos.poloidalModeNumbers = [0, 1, 2, 3, 4]
    default.boundary.RCos.toroidalModeNumbers = [-4, -3, -2, -1, 0, 1, 2, 3, 4]
    default.boundary.RCos.numRadialPoints = 1
    default.boundary.ZSin.coefficients = [
        4.5363e-04,
        3.1625e-04,
        3.5963e-04,
        8.3725e-03,
        -0.0000e00,
        -2.0666e-01,
        -4.1458e-03,
        -1.9493e-03,
        2.0185e-03,
        3.4400e-03,
        7.7506e-03,
        1.4961e-02,
        4.0227e-02,
        5.6892e-01,
        2.0596e-01,
        -5.7604e-03,
        -5.6140e-03,
        -4.2485e-03,
        4.5363e-04,
        3.1625e-04,
        3.5963e-04,
        8.3725e-03,
        1.1405e-03,
        2.3889e-02,
        -6.0502e-02,
        8.9796e-03,
        9.1004e-04,
        -3.7464e-04,
        -4.2385e-05,
        5.3668e-04,
        -1.7563e-03,
        -4.2733e-03,
        -4.4707e-03,
        9.5155e-03,
        1.0233e-02,
        -2.8137e-03,
        1.2480e-04,
        -8.7567e-05,
        7.6525e-05,
        6.1672e-04,
        3.6261e-03,
        -2.8280e-03,
        7.3549e-03,
        -5.6303e-03,
        -2.8346e-04,
    ]
    default.boundary.ZSin.poloidalModeNumbers = [0, 1, 2, 3, 4]
    default.boundary.ZSin.toroidalModeNumbers = [-4, -3, -2, -1, 0, 1, 2, 3, 4]
    default.boundary.ZSin.numRadialPoints = 1

    return default


class TestProfile(unittest.TestCase):
    locs = [0.0, 1.0]
    coef = [1.0, -1.0]

    not_supported_profiles = [GaussianProcess(domain=locs, coefficients=coef)]

    def test_not_supported_vmec_profile(self):
        for profile in self.not_supported_profiles:
            self.assertRaises(ValueError, to_osa_type, profile, ws_server=WS_SERVER)

    def test_power_series(self):
        profile = to_osa_type(PowerSeries(coefficients=self.coef), ws_server=WS_SERVER)

        self.assertEqual(profile.ProfileType, "power_series")
        self.assertListEqual(profile.coefficients, self.coef)

    def test_cubic_spline(self):
        profile = to_osa_type(
            CubicSpline(domain=self.locs, coefficients=self.coef), ws_server=WS_SERVER
        )

        self.assertEqual(profile.ProfileType, "cubic_spline")
        self.assertListEqual(profile.locations, self.locs)
        self.assertListEqual(profile.coefficients, self.coef)

    def test_cubic_spline_current(self):
        profile = to_osa_type(
            CubicSpline(domain=self.locs, coefficients=self.coef),
            ws_server=WS_SERVER,
            kind="current",
        )

        self.assertEqual(profile.ProfileType, "cubic_spline_I")
        self.assertListEqual(profile.locations, self.locs)
        self.assertListEqual(profile.coefficients, self.coef)

    def test_cubic_spline_current_density(self):
        profile = to_osa_type(
            CubicSpline(domain=self.locs, coefficients=self.coef),
            ws_server=WS_SERVER,
            kind="current_density",
        )

        self.assertEqual(profile.ProfileType, "cubic_spline_Ip")
        self.assertListEqual(profile.locations, self.locs)
        self.assertListEqual(profile.coefficients, self.coef)

    def test_two_power(self):
        coef = [1, 2, 2]
        profile = to_osa_type(TwoPower(coefficients=coef), ws_server=WS_SERVER)

        self.assertEqual(profile.ProfileType, "two_power")
        self.assertListEqual(profile.coefficients, coef)

    def test_sum_atan(self):
        coef = [0, 1, 2, 1, 2]
        profile = to_osa_type(SumAtan(coefficients=coef), ws_server=WS_SERVER)

        self.assertEqual(profile.ProfileType, "sum_atan")
        self.assertListEqual(profile.coefficients, coef)

    def test_cubic_spline_extended_locations(self):
        num_locations = 99
        locs_ = list(np.linspace(self.locs[0], self.locs[-1], num_locations))
        profile = CubicSpline(domain=self.locs, coefficients=self.coef)
        vmec_profile = to_osa_type(
            profile, ws_server=WS_SERVER, num_locations=num_locations
        )

        self.assertListEqual(vmec_profile.locations, locs_)
        self.assertListEqual(vmec_profile.coefficients, list(profile(locs_)))


class TestFourierCoefficients(unittest.TestCase):
    coef = [[[0, 1, 2], [3, 4, 5]], [[6, 7, 8], [9, 10, 11]]]
    series = Cos(coef, num_field_periods=5)

    def test_from_series(self):
        vmec_coef = to_osa_type(self.series, ws_server=WS_SERVER)
        self.assertListEqual(
            vmec_coef.coefficients, list(np.array(self.coef).flatten())
        )
        self.assertListEqual(vmec_coef.poloidalModeNumbers, [0, 1])
        self.assertListEqual(vmec_coef.toroidalModeNumbers, [-1, 0, 1])
        self.assertEqual(vmec_coef.numRadialPoints, 2)


class TestSurfaceCoefficients(unittest.TestCase):
    cos = [0.0, 5.5, 0.151, 0.039, 0.26, -0.085]
    sin = [0.0, 0.0, -0.154, 0.040, 0.45, 0.180]
    series = TensorSeries(
        Fourier(cos=Cos.from_coefficients(cos, mpol=1, ntor=1)),
        Fourier(sin=Sin.from_coefficients(sin, mpol=1, ntor=1)),
    )

    def test_from_series(self):
        vmec_surf = to_osa_type(self.series, ws_server=WS_SERVER)
        self.assertListEqual(vmec_surf.RCos.coefficients, self.cos)
        self.assertListEqual(vmec_surf.ZSin.coefficients, self.sin)
        self.assertListEqual(vmec_surf.RCos.poloidalModeNumbers, [0, 1])
        self.assertListEqual(vmec_surf.ZSin.toroidalModeNumbers, [-1, 0, 1])
        self.assertEqual(vmec_surf.RCos.numRadialPoints, 1)


class TestVmecInput(unittest.TestCase):
    #  VMEC input as from web_service docs
    ws_input = get_defaul_vmec_input()

    #  VMEC input as from w7x package
    w7x_default_input = get_default_vmec_input_from_state(free_boundary=True)
    w7x_from_state_input = VmecInput.from_state(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
        free_boundary=True,
    )

    vmec_server = get_server(WS_SERVER)

    def test_w7x_default_has_not_nones(self):
        osa_input = to_osa_type(self.w7x_default_input, ws_server=WS_SERVER)
        for name, value in self.ws_input.__dict__.items():
            if value is not None:
                value_ = getattr(osa_input, name)
                self.assertTrue(
                    value_ is not None,
                    msg=f"{name} attribute is None",
                )
                self.assertEqual(
                    type(value), type(value_), msg=f"{name} attribute has wrong type"
                )

    def test_w7x_from_state_has_not_nones(self):
        osa_input = to_osa_type(self.w7x_from_state_input, ws_server=WS_SERVER)
        for name, value in self.ws_input.__dict__.items():
            if value is not None:
                value_ = getattr(osa_input, name)
                self.assertTrue(
                    value_ is not None,
                    msg=f"{name} attribute is None",
                )
                self.assertEqual(
                    type(value), type(value_), msg=f"{name} attribute has wrong type"
                )

    @pytest.mark.web_service
    def test_ws_input_exec(self):
        vmec_id = "w7x_package_test_" + now()
        vmec_id_ = run_service(
            self.vmec_server.service.execVmec, self.ws_input, vmec_id
        )
        self.assertEqual(vmec_id, vmec_id_)
        sleep(1)  # Not to break the web_service

    @pytest.mark.web_service
    def test_ws_init_exec(self):
        vmec_id = "w7x_package_test_" + now()
        vmec_id_ = run_service(
            self.vmec_server.service.execVmec,
            to_osa_type(self.w7x_from_state_input, ws_server=WS_SERVER),
            vmec_id,
        )
        self.assertEqual(vmec_id, vmec_id_)
        sleep(1)  # Not to break the web_service

    def test_unique_vmec_id(self):
        vmec_input = get_default_vmec_input_from_state(free_boundary=True)
        vmec_input.vmec_id = None
        self.assertNotEqual(self.w7x_default_input.vmec_id, vmec_input.vmec_id)

    def test_to_string(self):
        indata = self.w7x_from_state_input.to_string()
        #  TODO-2(@amerlo): better way to check? Include string for complete file here
        #                 and check full string.
        #  TODO-2(@amerlo): check all the parameters!
        self.assertTrue(indata.startswith("&INDATA"), msg=indata)
        self.assertTrue("LFREEB = T\n" in indata, msg=indata)
        self.assertTrue("NS_ARRAY = 4 9 28 99\n" in indata, msg=indata)
        self.assertTrue("RAXIS_CC = 5.5496 0.43857\n" in indata, msg=indata)
        self.assertTrue("ZAXIS_CS = 0 -0.30789\n" in indata, msg=indata)


class TestWout(unittest.TestCase):
    """
    Test Wout object.
    """

    vmec_id = "w7x_ref_169"
    # TODO(@amerlo): make absolute, use config.get
    test_wout_file_path = os.path.join(
        "tests",
        "resources",
        "vmec",
        "wout_w7x.0972_0926_0880_0852_+0000_+0000.01.00jh.nc",
    )

    def setUp(self):
        self.vmec = VmecWebServiceBackend()
        self.wout = self.vmec.get_wout(self.vmec_id)

    def test_vmec_id(self):
        self.assertEqual(self.wout.file_id, self.vmec_id)

    def test_b_axis_fit(self):
        #  TODO-1(@amerlo): increase number of decimal!
        vmec_ids = ["w7x_ref_169", "w7x_ref_177", "w7x_ref_418"]
        for vmec_id in vmec_ids:
            wout = self.vmec.get_wout(vmec_id)
            for phi in np.linspace(0.0, 2 * np.pi / 5, num=10):
                wout_b0 = wout.b_axis(phi)
                b0 = self.vmec.get_b_axis(vmec_id, phi)
                np.testing.assert_almost_equal(
                    wout_b0,
                    b0,
                    err_msg=f"Magnetic axis at {phi} does not match for {vmec_id}.",
                    decimal=2,
                )

    def test_from_file(self):
        wout = Wout.load(self.test_wout_file_path, status=Status.SUCCESS)
        self.assertEqual(wout.file_id, "w7x.0972_0926_0880_0852_+0000_+0000.01.00jh")
        self.assertEqual(wout.status, Status.SUCCESS)
        self.assertEqual(int(wout.data["xm"][-1]), 11)
        self.assertEqual(int(wout.data["xm_nyq"][-1]), 11)
        np.testing.assert_almost_equal(wout.data["b0"][:], -2.4274001866915405)
        np.testing.assert_almost_equal(wout.data["phi"][-1], -1.775499999999998)
        np.testing.assert_almost_equal(
            wout.get_phi_as_profile()(1.0), -1.775499999999998
        )
        np.testing.assert_almost_equal(wout.get_betatotal(), 2.044179761810504e-13)

    def test_to_state(self):
        equi = self.wout.to_state().equilibrium
        self.assertEqual(equi.field_period, 5)
        np.testing.assert_almost_equal(equi.beta, 0.0065466974926590964)
        flux_surfaces = equi.flux_surfaces
        self.assertEqual(flux_surfaces.dims[0].cos.num_field_periods, 5)
        self.assertEqual(flux_surfaces.dims[1].sin.mpol, 11)
        self.assertEqual(flux_surfaces.dims[0].sin, None)


class TestGetRunStatusFromThreed1(unittest.TestCase):
    """
    Test the get_run_status_from_threed1 function.

    TODO-1(@amerlo): add test for BAD_JACOBIAN.
    """

    test_threed1_file_paths = [
        (
            os.path.join(
                "tests",
                "resources",
                "vmec",
                "threed1.w7x.0972_0926_0880_0852_+0000_+0000.01.00jh",
            ),
            Status.SUCCESS,
        ),
        (
            os.path.join(
                "tests",
                "resources",
                "vmec",
                "threed1.minerva-vmec-0376fc0aa736e3896b6d5b600837f175",
            ),
            Status.SUCCESS,  # TODO-1(@amerlo): Boundary warning here!
        ),
        (
            os.path.join(
                "tests",
                "resources",
                "vmec",
                "threed1.w7x-vmec-28b893158bbd075e8c6600fe6a0d12e2",
            ),
            Status.INCREASE_NITER,
        ),
        (
            os.path.join(
                "tests",
                "resources",
                "vmec",
                "threed1.w7x-vmec-d5e8cf967198c15c0ca1b0768dad6ec2",
            ),
            Status.UNKNOWN,
        ),
    ]

    def test_threed1_status(self):
        for threed1_path, threed1_status in self.test_threed1_file_paths:
            with open(threed1_path, "r") as f:
                threed1 = str(f.read())
            self.assertEqual(get_run_status_from_threed1(threed1), threed1_status)


class TestVmecWebServiceBackend(unittest.TestCase):
    """
    Test VMEC web service backend.
    """

    #  Use random pressure scale to create a unique vmec_id
    state = w7x.State(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(total_toroidal_current=np.random.random()),
        w7x.config.Equilibria.InitialGuess(),
    )
    vmec_id_not_succ = "w7x.1000_1000_1000_1000_+0500_+0500.06.088"
    vmec_id_succ = "w7x-package-test-20210311-103251"

    def setUp(self):
        self.vmec = VmecWebServiceBackend()

    def test_exists(self):
        self.assertTrue(self.vmec.exists(self.vmec_id_not_succ))
        self.assertTrue(not self.vmec.exists("i_do_not_exist"))

    def test_is_ready(self):
        self.assertTrue(self.vmec.is_ready(self.vmec_id_not_succ))

    def test_is_successful(self):
        self.assertTrue(self.vmec.is_successful(self.vmec_id_succ))

    def test_is_not_successful(self):
        self.assertTrue(not self.vmec.is_successful(self.vmec_id_not_succ))

    def test_get_wout(self):
        wout = self.vmec.get_wout(self.vmec_id_succ)
        self.assertEqual(wout.file_id, self.vmec_id_succ)
        self.assertEqual(wout.data["version_"][:], 9.0)
        self.assertEqual(wout.status, Status.SUCCESS)

    def test_status(self):
        status = self.vmec.get_status(self.vmec_id_succ)
        self.assertEqual(status, Status.SUCCESS)
        status = self.vmec.get_status(self.vmec_id_not_succ)
        self.assertEqual(status, Status.INCREASE_NITER)

    def test_get_threed1(self):
        threed1 = self.vmec.get_threed1(self.vmec_id_succ)
        self.assertTrue(f"SHOT ID.: {self.vmec_id_succ}" in threed1)

    def test_get_b_axis(self):
        b_axis = self.vmec.get_b_axis(self.vmec_id_succ)
        np.testing.assert_almost_equal(b_axis, 2.8903436552479245)

    @pytest.mark.web_service
    def test_execute(self):
        wout = self.vmec._compute(self.state, free_boundary=True, dry_run=False)

        # TODO-1(@dboe): Make unfail again. Or is it an error even?
        # vmec_id = VmecInput.from_state(self.state, free_boundary=True).vmec_id
        # self.assertEqual(wout.file_id, vmec_id)
        self.assertTrue(self.vmec.exists(wout.file_id))
        self.assertTrue(self.vmec.is_ready(wout.file_id))
        self.assertTrue(self.vmec.is_successful(wout.file_id))

    def test_raise_dry_run(self):
        self.assertRaises(
            TypeError, self.vmec._compute, self.state, free_boundary=True, dry_run=True
        )


class ProvideTempFolder:
    """
    Class to provide temp folder where to place test VMEC runs.
    """

    def setUp(self):
        self.folder = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.folder)


class TestVmecMockupBackend(ProvideTempFolder, unittest.TestCase):
    """
    Test VMEC mockup backend.
    """

    def setUp(self):
        super().setUp()
        w7x.config.vmec.mock.folder = self.folder
        self.vmec = Vmec(strategy="mock")

    def test_mock_up_beta_vacuum(self):
        state = self.vmec.free_boundary(w7x.State(w7x.config.Plasma.Vacuum()))
        np.testing.assert_almost_equal(state.equilibrium.beta, 0)

    def test_mock_up_beta_interpolation(self):
        beta_previous = 0.0
        for p0 in [1e2, 1e3, 1e4, 1e5, 1e6]:
            state = w7x.State(
                w7x.config.Plasma.Vacuum(
                    pressure_profile=TwoPower(coefficients=[p0, 1, 2], domain=[0, 1])
                )
            )
            state = self.vmec.free_boundary(state)
            self.assertTrue(state.equilibrium.beta > beta_previous)
            beta_previous = state.equilibrium.beta


class TestVmecSlurmBackend(ProvideTempFolder, unittest.TestCase):
    """
    Test VMEC Slurm backend.

    TODO-1(@amerlo): add example for completed but not successful vmec runs.
    """

    state = w7x.State(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
    )
    test_folder = os.path.join("tests", "resources", "vmec")
    test_vmec_id = "w7x_vmec_test"

    def setUp(self):
        super().setUp()
        w7x.config.vmec.slurm.folder = self.test_folder
        self.vmec = VmecRunnerBackend()
        self.vmec.runner = "slurm"

    def test_exists(self):
        self.assertTrue(self.vmec.exists(self.test_vmec_id))
        self.assertFalse(self.vmec.exists("i_do_not_exist"))

    def test_is_ready(self):
        self.assertTrue(self.vmec.is_ready(self.test_vmec_id))
        self.assertFalse(self.vmec.is_ready("i_do_not_exist"))

    def test_is_successful(self):
        self.assertTrue(self.vmec.is_successful(self.test_vmec_id))
        self.assertFalse(self.vmec.is_successful("i_do_not_exist"))

    def test_get_wout(self):
        wout = self.vmec.get_wout(self.test_vmec_id)
        self.assertEqual(wout.file_id, self.test_vmec_id)
        self.assertEqual(wout.data["version_"][:], 9.0)

    def test_run_dir_exists(self):
        #  Change runner folder
        w7x.config.vmec.slurm.folder = self.folder
        self.vmec.runner = "slurm"
        wout = self.vmec._compute(self.state, free_boundary=True, dry_run=True)
        self.assertTrue(os.path.isdir(os.path.join(self.folder, wout.file_id)))

    def test_run_slurm_exists(self):
        #  Change runner folder
        w7x.config.vmec.slurm.folder = self.folder
        self.vmec.runner = "slurm"
        wout = self.vmec._compute(self.state, free_boundary=True, dry_run=True)
        self.assertTrue(
            os.path.isfile(os.path.join(self.folder, wout.file_id, "slurm"))
        )

    def test_run_indata_exists(self):
        #  Change runner folder
        w7x.config.vmec.slurm.folder = self.folder
        self.vmec.runner = "slurm"
        wout = self.vmec._compute(self.state, free_boundary=True, dry_run=True)
        self.assertTrue(
            os.path.isfile(
                os.path.join(self.folder, wout.file_id, f"input.{wout.file_id}")
            )
        )


class TestVmec(ProvideTempFolder, unittest.TestCase):
    """
    Test VMEC with different backend.
    """

    state = w7x.State(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
    )
    backends = [
        ("web_service", VmecWebServiceBackend),
        ("mock", VmecMockupBackend),
        ("local", VmecRunnerBackend),
        # TODO: ("nn", VmecNNBackend),
    ]

    def setUp(self):
        super().setUp()
        w7x.config.vmec.local.folder = self.folder
        w7x.config.vmec.mock.folder = self.folder

    @pytest.mark.web_service
    def test_web_service_free_boundary(self):
        vmec = Vmec()
        vmec.backend = "web_service"
        state = vmec.free_boundary(self.state)
        # Dummy check to evaluate if the state has been updated
        self.assertNotEqual(state.equilibrium.beta, None)

    def test_mock_up_free_boundary(self):
        vmec = Vmec(strategy="mock")
        state = vmec.free_boundary(self.state)
        # Dummy check to evaluate if the state has been updated
        self.assertNotEqual(state.equilibrium.beta, None)

    def test_local_dry_run(self):
        vmec = Vmec(strategy="local")
        state = vmec.free_boundary(self.state, dry_run=True)
        vmec_id = state.resources.items[-1].file_id
        input_file_path = os.path.join(self.folder, vmec_id, f"input.{vmec_id}")
        self.assertTrue(os.path.isfile(input_file_path))

    def test_dry_run(self):
        for backend, type_ in self.backends:
            # The dry_run option is not supported by the web_service backend
            if backend == "web_service":
                continue
            vmec = Vmec(strategy=backend)
            self.assertEqual(type(vmec.backend), type_)
            state = vmec.free_boundary(self.state, dry_run=True)
            #  Check for wout file under resources
            self.assertEqual(len(state.resources.items), 1)
            wout = state.resources.items[-1]
            self.assertEqual(wout.status, Status.NOT_STARTED)

    def test_switch_backend(self):
        vmec = Vmec()
        for backend, type_ in self.backends:
            vmec.backend = backend
            self.assertEqual(type(vmec.backend), type_)

    def test_runner_backend_init(self):
        vmec = Vmec(strategy="local")
        self.assertEqual(vmec.backend.folder, self.folder)


class TestVmecConverge(ProvideTempFolder, unittest.TestCase):
    """
    Test VMEC converge method with mock backend.
    """

    def setUp(self):
        super().setUp()
        w7x.config.vmec.mock.folder = self.folder
        self.vmec = Vmec(strategy="mock")
        # Default state which should converge in 1 iteration
        self.state = w7x.State(
            w7x.config.CoilSets.Ideal(),
            w7x.config.Plasma.Vacuum(),
            w7x.config.Equilibria.InitialGuess(),
        )

    # Free boundary equilibrium with the default state should
    # converge in 1 iteration with no warnings.
    def test_converge_success_one_iteration(self):
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            dry_run=False,
            iterations=1,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.SUCCESS)

    # Free boundary equilibrium with large phi edge should lead to
    # boundary warnings.
    def test_converge_boundary_one_iteration(self):
        self.state.equilibrium.phi = PowerSeries(coefficients=[0, -3.0], domain=[0, 1])
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            dry_run=False,
            iterations=1,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.BOUNDARY)

    def test_converge_boundary_n_iterations(self):
        self.state.equilibrium.phi = PowerSeries(coefficients=[0, -3.0], domain=[0, 1])
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            dry_run=False,
            iterations=5,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.SUCCESS)

    def test_converge_niter_one_iteration(self):
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            num_iter=1000,
            dry_run=False,
            iterations=1,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.INCREASE_NITER)

    def test_converge_niter_n_iterations(self):
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            num_iter=1000,
            dry_run=False,
            iterations=5,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.SUCCESS)

    def test_converge_timeout_one_iteration(self):
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            force_tolerance_levels=[1e-3, 1e-5, 1e-9, 1e-18],
            dry_run=False,
            iterations=1,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.TIMEOUT)

    def test_converge_timeout_n_iterations(self):
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            force_tolerance_levels=[1e-3, 1e-5, 1e-9, 1e-18],
            dry_run=False,
            iterations=5,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.SUCCESS)

    #  Make sure we do not propose a non monotonic force tolerance levels list
    #  This should not converge since force_tolerance_levels[-2] is already too low
    def test_converge_timeout_n_iterations_limit(self):
        wout = self.vmec._converge(
            self.state,
            free_boundary=True,
            force_tolerance_levels=[1e-3, 1e-5, 1e-17, 1e-18],
            dry_run=False,
            iterations=5,
        )
        self.assertEqual(wout.status, w7x.simulation.vmec.Status.TIMEOUT)


class TestVmecSearch(ProvideTempFolder, unittest.TestCase):
    """
    Test VMEC search method with mock backend.
    """

    def setUp(self):
        super().setUp()
        w7x.config.vmec.mock.folder = self.folder
        self.vmec = Vmec(strategy="mock")

    def test_search_null_beta(self):
        state = w7x.State(
            w7x.config.CoilSets.Ideal(),
            w7x.config.Plasma.Vacuum(),
            w7x.config.Equilibria.InitialGuess(),
        )
        state = self.vmec.search(state, beta=0.0)
        np.testing.assert_almost_equal(state.equilibrium.beta, 0.0)

    def test_search_target_beta(self):
        decimal = 3
        for I1 in [1.2e4, 1.3e4, 1.5e4]:
            for beta in [2e-3, 1e-2, 2e-2, 5e-2, 1e-1]:
                state = w7x.State(
                    w7x.config.CoilSets.Ideal.from_currents(
                        *[I1] * 5 + [0.0, 0.0], unit="A"
                    ),
                    w7x.config.Plasma.Vacuum(),
                    w7x.config.Equilibria.InitialGuess(),
                )
                state = self.vmec.search(
                    state, beta=beta, beta_threshold=10 ** (-decimal)
                )
                np.testing.assert_almost_equal(
                    state.equilibrium.beta, beta, decimal=decimal
                )

    def test_search_target_beta_and_b_axis(self):
        b_axis = 2.52
        beta_decimal = 3
        b_axis_decimal = 2
        for I1 in [1.2e4, 1.3e4, 1.5e4]:
            for beta in [2e-3, 1e-2, 2e-2, 5e-2, 1e-1]:
                state = w7x.State(
                    w7x.config.CoilSets.Ideal.from_currents(
                        *[I1] * 5 + [0.0, 0.0], unit="A"
                    ),
                    w7x.config.Plasma.Vacuum(),
                    w7x.config.Equilibria.InitialGuess(),
                )
                state = self.vmec.search(
                    state,
                    beta=beta,
                    b_axis=b_axis,
                    beta_threshold=10 ** (-beta_decimal),
                    b_axis_threshold=10 ** (-b_axis_decimal),
                )
                np.testing.assert_almost_equal(
                    state.equilibrium.beta, beta, decimal=beta_decimal
                )
                np.testing.assert_almost_equal(
                    state.equilibrium.b_axis, b_axis, decimal=b_axis_decimal
                )


class TestValidatePhiEdge(unittest.TestCase):
    """
    Test phi edge estimate heuristic.
    """

    wout_file_path = os.path.join(
        "tests",
        "resources",
        "vmec",
        "wout_w7x.0972_0926_0880_0852_+0000_+0000.01.00jh.nc",
    )

    def setUp(self):
        self.wout = Wout.load(self.wout_file_path)

    #  Test phi edge value is ok
    def test_default_kwargs(self):
        phi_edge = validate_phi_edge(
            major_radius=self.wout.get_rmajor_p(),
            plasma_volume=self.wout.get_volume_p(),
            phi=self.wout.get_phi(),
            iota=self.wout.get_iotaf(),
        )
        np.testing.assert_almost_equal(phi_edge, self.wout.get_phi()[-1])

    def test_smaller_volume(self):
        phi_edge = validate_phi_edge(
            major_radius=self.wout.get_rmajor_p(),
            plasma_volume=self.wout.get_volume_p(),
            phi=self.wout.get_phi(),
            iota=self.wout.get_iotaf(),
            max_volume=26.0,  # wout volume is ~26.1
        )
        self.assertTrue(np.abs(phi_edge) < np.abs(self.wout.get_phi()[-1]))

    def test_larger_volume(self):
        phi_edge = validate_phi_edge(
            major_radius=self.wout.get_rmajor_p(),
            plasma_volume=self.wout.get_volume_p(),
            phi=self.wout.get_phi(),
            iota=self.wout.get_iotaf(),
            min_volume=28.0,  # wout volume is ~26.1
        )
        self.assertTrue(np.abs(phi_edge) > np.abs(self.wout.get_phi()[-1]))

    def test_not_valid_volumes(self):
        self.assertRaises(
            ValueError,
            validate_phi_edge,
            major_radius=self.wout.get_rmajor_p(),
            plasma_volume=self.wout.get_volume_p(),
            phi=self.wout.get_phi(),
            iota=self.wout.get_iotaf(),
            max_volume=27.0,
            min_volume=28.0,
        )

    def test_not_valid_profiles(self):
        self.assertRaises(
            ValueError,
            validate_phi_edge,
            major_radius=self.wout.get_rmajor_p(),
            plasma_volume=self.wout.get_volume_p(),
            phi=self.wout.get_phi(),
            iota=self.wout.get_iotaf()[:-1],  # Remove last element
            max_volume=27.0,
            min_volume=28.0,
        )


class TestGetP0VsBeta(unittest.TestCase):
    """
    Test get_p0_estimate_from_fit function.
    """

    test_beta = [0.0, 0.01, 0.03, 0.05, 0.1]
    test_I1 = [1.2e4, 1.3e4, 1.4e4, 1.5e4]
    test_p0 = [0, 1e3, 1e4, 1e5, 2e5]
    decimal = 3

    #  Assume EJM+252 configuration
    p0s = np.linspace(0, 2e5, num=10)
    betas = (
        np.linspace(0, 0.03, num=10)
        + np.random.uniform(low=-1.0, high=1.0, size=10) / 10**decimal
    )
    betas[0] = 0.0

    def test_educated_guess(self):
        for beta in self.test_beta:
            for I1 in self.test_I1:
                p0 = _get_p0_vs_beta(pressure=None, beta=None, I1=I1)(beta)
                np.testing.assert_almost_equal(
                    p0 * 0.03 / 2e5 * (13067 / I1) ** 2, beta
                )

    def test_linear_fit(self):
        for beta in self.test_beta:
            p0 = _get_p0_vs_beta(pressure=self.p0s, beta=self.betas)(beta)
            #  Use decimal - 1 to ease the test
            np.testing.assert_almost_equal(
                p0 * 0.03 / 2e5, beta, decimal=self.decimal - 1
            )

    def test_inverse_linear_fit(self):
        for p0 in self.test_p0:
            beta = _get_p0_vs_beta(
                pressure=self.p0s, beta=self.betas, return_inverse=True
            )(p0)
            #  Use decimal - 1 to ease the test
            np.testing.assert_almost_equal(
                beta, p0 * 0.03 / 2e5, decimal=self.decimal - 1
            )
