import pytest
import unittest
import w7x
from w7x.lib.profiles import (
    PowerSeries,
)
import w7x.simulation.vmec
from w7x.simulation.backends.nn.vmec import VmecNNBackend

# TODO-1(@dboe): restructure the whole vmec test
# skip test if torch is not installed
pytest.importorskip(
    "torch"
)  # use torch as indicator for the network model. TODO-1(@dboe) check for checkpoints instead


class TestVmecNNBackend(unittest.TestCase):
    """Test VMECNN backend."""

    state = w7x.State(
        w7x.config.CoilSets.Ideal.from_currents(
            15000, 15000, 15000, 15000, 15000, 0, 0, unit="A"
        ),
        w7x.config.Plasma.Vacuum(),
        w7x.config.Equilibria.InitialGuess(),
    )

    def setUp(self):
        if w7x.config.vmec.nn.checkpoint is None or w7x.config.vmec.nn.metadata is None:
            self.skipTest("Model checkpoint or metadata are not set")
        self.vmec = VmecNNBackend()

    def test_unscaled_values_in_wout(self):
        wout = self.vmec._compute(self.state, free_boundary=True, dry_run=False)
        self.assertTrue(
            wout.data["phi"][-1] == self.state.equilibrium.phi.coefficients[1]
        )
        self.assertTrue(
            wout.data["presf"][0]
            == self.state.plasma_parameters.pressure_profile.coefficients[0]
        )

    def test_accuracy(self):
        configs = {
            "w7x_ref_3": {
                "extcur": [15000] * 5 + [0] * 2,
                "phiedge": -2.1907427,
                "pressure": [1e-6, -1e-6],
                "curtor": 0.0,
                "iota_axis": 0.856,
                "iota_edge": 0.968,
                "volume_p": 28.598786069587568,
                "raxis": 5.948689212362945,
                "vacuum_well": 0.01032587564605251,
            },
            "w7x_ref_9": {
                "extcur": [15000] * 5 + [0] * 2,
                "phiedge": -2.21,
                "pressure": [120000.0, -240000.0, 120000.0],
                "curtor": 0.0,
                "iota_axis": 0.8625986823520795,
                "iota_edge": 0.9674749573107098,
                "volume_p": 29.157422461602216,
                "raxis": 5.987294897443914,
                "vacuum_well": 0.04149533786894518,
            },
            "w7x_ref_183": {
                "extcur": [14188] * 5 + [-9790] * 2,
                "phiedge": -1.721525,
                "pressure": [9e4, -2 * 9e4, 9e4],
                "curtor": 0.0,
                "iota_axis": 1.009230869937641,
                "iota_edge": 1.2083603142833712,
                "volume_p": 26.5128201938992,
                "raxis": 5.995420235490623,
                "vacuum_well": 0.04283457757228531,
            },
        }

        for name, config in configs.items():
            state = w7x.State(
                w7x.config.CoilSets.Ideal.from_currents(*config["extcur"], unit="A"),
                w7x.config.Plasma.Vacuum(
                    pressure_profile=PowerSeries(coefficients=config["pressure"])
                ),
                w7x.config.Equilibria.InitialGuess(
                    phi=PowerSeries(coefficients=[0, config["phiedge"]])
                ),
            )
            wout = self.vmec._compute(state, free_boundary=True, dry_run=False)

            self.assertTrue(abs(wout.data["iotaf"][0] - config["iota_axis"]) < 1e-2)
            self.assertTrue(abs(wout.data["iotaf"][-1] - config["iota_edge"]) < 1e-2)
            self.assertTrue(abs(wout.data["volume_p"][:] - config["volume_p"]) < 2e-1)
            self.assertTrue(abs(wout.data["rmnc"][0].sum() - config["raxis"]) < 1e-3)
            self.assertTrue(abs(wout.vacuum_well() - config["vacuum_well"]) < 3e-3)
