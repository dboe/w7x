import pytest
import rna.pattern.link
import w7x
from w7x.model import MGrid
from w7x.simulation.vmec import VmecInput


def test_free_boundary_requires_mgrid():
    # free_boundary requires mgrid
    basic = w7x.State.merged(
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
    )

    # this requires the file to be there, so vmec_local must have been successfull
    VmecInput.from_state(
        basic, w7x.config.Equilibria.InitialGuess(), free_boundary=True
    )

    equi_wrong = w7x.config.Equilibria.InitialGuess(
        vacuum_field=rna.pattern.link.Link(
            ref=MGrid(path="wrong.nc"),
            fget=MGrid.to_numpy,
        )
    )
    with pytest.raises(NotImplementedError):
        VmecInput.from_state(basic, equi_wrong, free_boundary=True)

    # mgrid not required if free_boundary=False
    VmecInput.from_state(basic, equi_wrong, free_boundary=False)
