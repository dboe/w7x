"""Test for profiles."""

import unittest
import numpy as np
from scipy import interpolate

from tfields import TensorFields

from w7x.lib.profiles import (
    PowerSeries,
    TwoPower,
    CubicSpline,
    SumAtan,
    GaussianProcess,
    tanh,
    HigdonSwallKernKernel,
)


LEN = 10


class ProfileSignatureTests:
    """
    Generic tests for the signature of the __call__ magic method.
    """

    def test_call_scalar_shape(self):
        profile = self.get_profile(coefficients=self.coef)
        x = (profile.domain[0] + profile.domain[-1]) / 2
        assert not hasattr(x, "len")
        self.assertFalse(hasattr(profile(x), "len"))

    def test_call_array_size_one_shape(self):
        profile = self.get_profile(coefficients=self.coef)
        x = np.array((profile.domain[0] + profile.domain[-1]) / 2)
        assert x.size == 1
        self.assertTrue(profile(x).size, 1)

    def test_call_array_shape(self):
        shape = (LEN,)
        profile = self.get_profile(coefficients=self.coef)
        x = np.linspace(profile.domain[0], profile.domain[-1], num=shape[0])
        self.assertTrue(profile(x).shape, shape)

    def test_call_list(self):
        profile = self.get_profile(coefficients=self.coef)
        x = list(np.linspace(profile.domain[0], profile.domain[-1], num=LEN))
        self.assertTrue(profile(x).shape, (len(x),))


class ProfileCallTests:
    """
    Generic test functions for the __call__ magic method.
    """

    def test_call_scalars(self):
        for coef in self.coefs:
            profile = self.get_profile(coefficients=self.coef)
            call_fn = self.get_call_fn(coefficients=self.coef)
            domain = profile.domain
            for x in np.linspace(domain[0], domain[-1], num=LEN):
                np.testing.assert_almost_equal(call_fn(x), profile(x))


class ProfileFitTests:
    """
    Generic test functions for the fit class method.

    TODO-2(@amerlo): increase noise resiliance.
    """

    decimal = 2

    def test_noisy_fit(self):
        call_fn = self.get_call_fn(self.coef)
        x = np.linspace(0, 1, num=LEN)
        y = call_fn(x) + 10 ** (-self.decimal - 1) * np.random.normal(size=x.size)
        profile = self.get_fit(x=x, y=y, deg=len(self.coef) + 1)
        domain = profile.domain
        for x in np.linspace(domain[0], domain[-1], num=LEN):
            np.testing.assert_almost_equal(profile(x), call_fn(x), decimal=self.decimal)

    def test_fit_from_tensor_fields(self):
        #  Linear function f(x) = 1 - x
        #  All profile types should be able to fit this
        x = np.linspace(0, 1, num=LEN)
        y = 1 - x
        field = TensorFields(x, y)
        profile = self.get_fit(x=field, deg=1)
        domain = profile.domain
        for x in np.linspace(domain[0], domain[-1], num=LEN):
            np.testing.assert_almost_equal(profile(x), 1 - x, decimal=self.decimal)


class ProfileOperatorsTests:
    """
    Generic test functions for the profile algebraic operators.
    """

    def test_mul(self):
        base_profile = self.get_profile(self.coef)
        call_fn = self.get_call_fn(self.coef)
        for factor in [-2.0, -0.5, 0.1, 0.5, 1, 2, 10.0]:
            profile = base_profile * factor
            domain = profile.domain
            for x in np.linspace(domain[0], domain[-1], num=LEN):
                np.testing.assert_almost_equal(profile(x), call_fn(x) * factor)


class TestPowerSeries(
    ProfileSignatureTests,
    ProfileCallTests,
    ProfileOperatorsTests,
    ProfileFitTests,
    unittest.TestCase,
):
    """
    Test for a PowerSeries profile.
    """

    #  Fixed test coefficients
    coef = [1.0, 0.1, -0.1]

    #  Generate random coefficients to test
    coefs = [list(np.random.uniform(low=-1.0, high=1.0, size=i + 1)) for i in range(10)]

    @staticmethod
    def get_call_fn(coefficients):
        return np.polynomial.polynomial.Polynomial(coef=coefficients)

    @staticmethod
    def get_profile(*args, **kwargs):
        return PowerSeries(*args, **kwargs)

    @staticmethod
    def get_fit(*args, **kwargs):
        return PowerSeries.fit(*args, **kwargs)

    ####################
    # CLASS ONLY TESTS #
    ####################

    def test_linear_fit(self):
        x = [0, 0.5, 1]
        y = [1, 0.5, 0]
        profile = self.get_fit(x, y, deg=1)
        domain = profile.domain
        for x in np.linspace(domain[0], domain[-1], num=LEN):
            np.testing.assert_almost_equal(profile(x), 1 - x)


class TestTwoPower(
    ProfileSignatureTests,
    ProfileCallTests,
    ProfileOperatorsTests,
    ProfileFitTests,
    unittest.TestCase,
):
    """
    Test for a TwoPower profile.
    """

    #  Fixed test coefficients
    coef = [1, 2, 2]

    #  Generate random coefficients to test
    coefs = [list(np.random.uniform(low=-1.0, high=1.0, size=3)) for i in range(10)]

    @staticmethod
    def get_call_fn(coefficients):
        return lambda x: coefficients[0] * (1 - x ** coefficients[1]) ** coefficients[2]

    @staticmethod
    def get_profile(*args, **kwargs):
        return TwoPower(*args, **kwargs)

    @staticmethod
    def get_fit(*args, **kwargs):
        return TwoPower.fit(*args, **kwargs)

    ####################
    # CLASS ONLY TESTS #
    ####################

    def test_quadratic_fit(self):
        x = [0, 0.75, 0.5, 0.25, 1]
        y = [1, 1 / 16, 0.25, 9 / 16, 0]
        profile = self.get_fit(x, y)
        domain = profile.domain
        for x in np.linspace(domain[0], domain[-1], num=LEN):
            np.testing.assert_almost_equal(profile(x), (1 - x) ** 2)


class TestCubicSpline(
    ProfileSignatureTests,
    ProfileCallTests,
    ProfileOperatorsTests,
    ProfileFitTests,
    unittest.TestCase,
):
    """
    Test for a CubicSpline profile.
    """

    #  Fixed test coefficients
    domain = np.linspace(0, 1, num=LEN)
    coef = domain**2

    #  Generate random coefficients to test
    @property
    def coefs(self):
        return [
            list(np.random.uniform(low=-1.0, high=1.0, size=self.domain.size))
            for i in range(10)
        ]

    def get_call_fn(self, coefficients):
        return lambda x: interpolate.CubicSpline(
            x=self.domain, y=coefficients, extrapolate=True
        )(x)

    def get_profile(self, *args, **kwargs):
        return CubicSpline(*args, domain=self.domain, **kwargs)

    @staticmethod
    def get_fit(*args, **kwargs):
        return CubicSpline.fit(*args, **kwargs)


class TestSumAtan(
    ProfileSignatureTests,
    ProfileCallTests,
    ProfileOperatorsTests,
    ProfileFitTests,
    unittest.TestCase,
):
    """
    Test for a SumAtan profile.
    """

    #  Fixed test coefficients
    coef = [0, 1, 1, 1, 1]

    #  Generate random coefficients to test
    coefs = [list(np.random.uniform(low=-1.0, high=1.0, size=5)) for i in range(10)]

    @staticmethod
    def get_call_fn(coefficients):
        return lambda x: coefficients[0] + 2 / np.pi * coefficients[1] * np.arctan(
            np.divide(
                coefficients[2] * np.array(x) ** coefficients[3],
                (1 - np.array(x)) ** coefficients[4],
            )
        )

    @staticmethod
    def get_profile(*args, **kwargs):
        return SumAtan(*args, **kwargs)

    @staticmethod
    def get_fit(*args, **kwargs):
        return SumAtan.fit(*args, **kwargs)


class TestGaussianProcess(ProfileSignatureTests, unittest.TestCase):
    """Test for a GaussianProcess profile."""

    #  Fixed test coefficients
    domain = np.linspace(0, 1, num=LEN)
    coef = domain**2

    #  Generate random coefficients to test
    @property
    def coefs(self):
        return [
            list(np.random.uniform(low=-1.0, high=1.0, size=self.domain.size))
            for i in range(10)
        ]

    def get_call_fn(self, coefficients):
        raise NotImplementedError

    def get_profile(self, *args, **kwargs):
        return GaussianProcess(*args, domain=self.domain, **kwargs)

    @staticmethod
    def get_fit(*args, **kwargs):
        return GaussianProcess.fit(*args, **kwargs)

    ####################
    # CLASS ONLY TESTS #
    ####################

    def test_assert_more_than_one_features(self):
        #  GaussianProcess supports only 1d array as x
        domain = self.domain.reshape(1, -1)
        self.assertRaises(
            NotImplementedError, GaussianProcess, domain=domain, coefficients=self.coef
        )

    def test_assert_more_than_one_targets(self):
        #  GaussianProcess supports only 1d array as y
        values = np.asarray(self.coef).reshape(1, -1)
        self.assertRaises(NotImplementedError, self.get_profile, coefficients=values)

    def test_quadratic_fit(self):
        x = [0, 0.75, 0.5, 0.25, 1]
        y = [1, 1 / 16, 0.25, 9 / 16, 0]
        profile = self.get_fit(x, y)
        domain = profile.domain
        for x in np.linspace(domain[0], domain[-1], num=LEN):
            np.testing.assert_almost_equal(profile(x), (1 - x) ** 2, decimal=3)

    def test_update_fit(self):
        gp = self.get_profile(coefficients=1 - np.linspace(0, 1, num=LEN))
        #  Update coefficients and check profile samples
        gp.coefficients = self.coef
        np.testing.assert_almost_equal(gp(self.domain), self.coef, decimal=4)

    def test_do_not_update_fit(self):
        gp = self.get_profile(coefficients=self.coef)
        x_id = id(gp._gp.X_train_)
        gp(self.domain)
        self.assertEqual(x_id, id(gp._gp.X_train_))

    def test_sample_y(self):
        gp = self.get_profile(coefficients=self.coef)
        samples = gp.sample_y(self.domain, n_samples=10)
        for i, a in enumerate(samples):
            for j, b in enumerate(samples):
                if i == j:
                    continue
                self.assertTrue(not np.all(np.equal(a, b)))

    def test_sample_y_with_constraints(self):
        constraints = [lambda x: x >= 0, lambda x: x <= 1]
        gp = self.get_profile(coefficients=self.coef, constraints=constraints)
        samples = gp.sample_y(self.domain, n_samples=100)
        self.assertEqual(len(samples), 100)
        for sample in samples:
            for constraint_fn in constraints:
                self.assertTrue(constraint_fn(sample).all())

    def test_sample_y_one_sample(self):
        constraints = [lambda x: x >= 0, lambda x: x <= 1]
        gp = self.get_profile(coefficients=self.coef, constraints=constraints)
        samples = gp.sample_y(self.domain, n_samples=1)
        self.assertEqual(len(samples), 1)
        for sample in samples:
            for constraint_fn in constraints:
                self.assertTrue(constraint_fn(sample).all())


class TestHigdonSwallKernKernel(unittest.TestCase):
    """Test for the HigdonSwallKern kernel."""

    hps = (0.8, 0.2, 0.5, 0.1)
    Xs = [[-1e6, 1e6], [0, 1], [1, 0], [0.6, 0.4]]
    Ks = [
        [[1, 0], [0, 1]],
        [[1, 0.15763552], [0.15763552, 1]],
        [[1, 0.15763552], [0.15763552, 1]],
        [[1, 0.7572159], [0.7572159, 1]],
    ]

    def setUp(self):
        self.kernel = HigdonSwallKernKernel(*self.hps)

    def test_tanh(self):
        l1, l2, l0, lw = self.hps
        #  tanh(l0)
        self.assertEqual(tanh(l0, l1, l2, l0, lw), (l1 + l2) / 2)
        #  tanh(+inf)
        np.testing.assert_almost_equal(tanh(1e6, l1, l2, l0, lw), l2)
        #  tanh(-inf)
        np.testing.assert_almost_equal(tanh(-1e6, l1, l2, l0, lw), l1)

    def test_call(self):
        for X, K in zip(self.Xs, self.Ks):
            X = np.array(X).reshape(-1, 1)
            np.testing.assert_almost_equal(self.kernel(X), K)

    def test_is_stationary(self):
        self.assertTrue(self.kernel.is_stationary() is False)
