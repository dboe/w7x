import datetime
import filecmp
import os
import pathlib
import shutil

try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

__source_dir = "docs"
pyproject_path = os.path.join(os.path.dirname(__file__), "../pyproject.toml")
with open(pyproject_path, "rb") as f:
    __config = tomllib.load(f)

project = __config["tool"]["poetry"]["name"]
author = __config["tool"]["poetry"]["authors"][0]
copyright = str(datetime.date.today().year) + ", " + author
release = __config["tool"]["poetry"]["version"]

# copy the ../REAMDE.rst to the docs/_dynamic folder
readme_path = pathlib.Path("..") / "README.rst"
dynamic_dir = pathlib.Path(".") / "_dynamic"
readme_dynamic_path = dynamic_dir / "README.rst"
os.makedirs(str(dynamic_dir), exist_ok=True)
# check diffs of README.rst and _dynamic/README.rst for serve mode
if not os.path.exists(readme_dynamic_path) or not filecmp.cmp(
    str(readme_path), str(readme_dynamic_path)
):
    shutil.copy(str(readme_path), str(readme_dynamic_path))

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_design",
    "sphinx_copybutton",
    "sphinxcontrib.mermaid",
    "sphinx.ext.viewcode",  # Add links to highlighted source code
    "sphinx.ext.doctest",  # Test snippets in the documentation
    "sphinx.ext.autosummary",
    "sphinx_autodoc_typehints",  # Automatically parse type hints
]
autosummary_generate = True  # Turn on sphinx.ext.autosummary

templates_path = ["_templates"]
exclude_patterns = ["_build"]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
}

napoleon_google_docstring = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = False
napoleon_attr_annotations = True

html_show_sourcelink = (
    # Remove 'view source code' from top of page (for html, not python)
    False
)
# If no docstring, inherit from base class
autodoc_inherit_docstrings = True
# Enable 'expensive' imports for sphinx_autodoc_typehints
set_type_checking_flag = True
# Continue through Jupyter errors
nbsphinx_allow_errors = True
# # Sphinx-native method. Not as good as sphinx_autodoc_typehints
# autodoc_typehints = "description"
# Remove namespaces from class/method signatures
add_module_names = False
autoclass_content = (
    # Both the class’ and the __init__ method’s docstring are concatenated
    "both"
)
autodoc_class_signature = "separated"
autodoc_default_options = {
    "show-inheritance": True,
}
autodoc_member_order = "bysource"
autodoc_typehints_format = "short"

always_document_param_types = True
typehints_defaults = "comma"

auto_pytabs_min_version = (3, 7)
auto_pytabs_max_version = (3, 11)

autosectionlabel_prefix_document = True

suppress_warnings = ["autosectionlabel.*"]

html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]
html_css_files = ["style.css"]
html_js_files = ["versioning.js"]
html_favicon = "images/favicon.ico"
html_logo = "images/logo.svg"
html_show_sourcelink = False
html_sidebars = {"about/*": []}
html_title = f"{project} Framework"

if "github" in __config["tool"]["poetry"]["repository"]:
    remote_service = "github"
else:
    remote_service = "gitlab"
html_theme_options = {
    "use_edit_page_button": False,
    "show_toc_level": 4,
    "icon_links": [
        {
            "name": "Source",
            "url": __config["tool"]["poetry"]["repository"],
            "icon": f"fa-brands fa-{remote_service}",
            "type": "fontawesome",
        },
    ],
    "navbar_center": ["navbar-nav"],
}
