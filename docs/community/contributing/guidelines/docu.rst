Documentation
-------------

Use examples in code-fences ('.. code-block:: language-mode') or doctest prompts ('>>>') extensively but on a high level and not with too much detail.
Details are to be tested systematically in pytests.
