Documentation
-------------

To compile the documentation (including automatically generated module api docs), run

    .. code-block:: shell

        pixi run docs
    
To build the documentation and run a live-reloading server, use the command 

    .. code-block:: shell

        pixi run docs-serve

This will start a server that will rebuild the documentation whenever a change is made, and display it in your web browser.
