=============
API reference
=============

..
    How to dive deeper? Give me the api?
    [reference]

    
.. autosummary::
    :toctree: _autosummary
    :template: custom-module-template.rst
    :recursive:

    w7x

The complete display of submodules is found in the toctree or `here <_autosummary/w7x.html>`_ 
