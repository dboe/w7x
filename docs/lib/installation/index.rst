============
Installation
============
..
    How to get it and set it up?
    [installation]

To install w7x, you have the following options:

.. tab-set::

    .. tab-item:: PyPi :octicon:`package`

        The preferred method to install w7x is to get the most recent stable release from PyPi:
        
        .. include:: ../../_dynamic/README.rst
            :start-after: [install-start]
            :end-before: [install-end]

        If you don't have `pip`_ installed, this `Python installation guide`_ can guide you through the process.
        
        .. _pip: https://pip.pypa.io
        .. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/

        .. dropdown:: Extras
            :icon: star
            
            Install a special extra:
                :code:`pip install w7x[extra]`

            All extras:
                :code:`pip install w7x[full]`

    .. tab-item:: Source :octicon:`code`

        First you have to retrieve the source code of w7x.
        You have the following options:
        
        .. tab-set::
        
            .. tab-item:: Git :octicon:`git-branch`

                To clone the public repository run

                .. code-block:: shell

                    git clone git://gitlab.mpcdf.mpg.de/dboe/w7x

            .. tab-item:: Tarball :octicon:`gift`

                Either download the tarball `here <https://gitlab.mpcdf.mpg.de/dboe/w7x/tarball/master>`_ or run

                .. code-block:: shell

                    curl -OJL https://gitlab.mpcdf.mpg.de/dboe/w7x/tarball/master

                
        Once you have a copy of the source, navigate inside and install it with:

        .. code-block:: shell

            poetry install


Backends Requirements
=====================

Most backends run out of the box.
However, some backends require a setup/instalation porcess that can not be (safely) done via pip.
Typically only 'local' and 'nn' backends require setup.
If implemented the 'web_service' backends should run.
The 'slurm' backend requires the proper setup on the supercomputer you are running.

If a setup is required, the following unified signature is used:

.. code-block:: shell
   
    poetry run install_<code>_<backend>
   
where <code> is the name of the code and <backend> is the name of the backend.

In the following we describe the codes & backends that require additional setup.

----
VMEC
----

Local
-----

Install VMEC2000 via `Stellopt`_.


.. code-block:: shell
   
    
    poetry run install_vmec_local


Neural Network (NN)
-------------------

For the Wendelstein 7-X machine we have approximated the parameter space by a NN model, as reported in [Merlo2023].
Install the NN addon via

.. code-block:: shell
   
    poetry install --with=vmec_nn
    poetry run install_vmec_nn
   
Note: If you run into certificate problems (not yet solved by it), you can circumvent those by using the system-git-client

.. code-block:: shell

    poetry config experimental.system-git-client true

.. [Merlo2023]:
    Merlo, Andrea, Daniel Böckenhoff, Jonathan Schilling, Samuel Aaron Lazerson, Thomas Sunn Pedersen, and the W7-X. Team. “Physics-Regularized Neural Network of the Ideal-MHD Solution Operator in Wendelstein 7-X Configurations.” Nuclear Fusion 63, no. 6 (April 2023): 066020. https://doi.org/10.1088/1741-4326/acc852.

-----------------------
Field Line Tracer (flt)
-----------------------

FieldLineTracer by Sergey Bozhenkov

Local
-----

* Note: this is currently in development and not yet present.

.. code-block:: shell
   
    poetry run install_flt_local


clone git repo to local dir

```
git clone https://git.ipp-hgw.mpg.de/boz/fieldlinetracer.git
```
follow the instructions in the file 'INSTALL'

install external libraries mentioned there:
```
sudo apt-get install libboost-test-dev
sudo apt-get install doxygen
sudo apt-get install libgsl-dev
sudo apt-get install gsoap
```

----------
Fieldlines
----------

Field line tracer included in stellopt.
Install Fieldlines via `Stellopt`_.

* Note: this is currently in development and not yet present.

.. code-block:: shell
   
    poetry run install_fieldlines_local


.. _Stellopt: https://github.com/PrincetonUniversity/STELLOPT


Presentaion Requirements
========================

If you want to contribute to the presentation, please install the extras and set up jupyter via

.. code-block:: shell

    poetry install --with=presentation
    poetry run install_presentation
