Usage
=====
..
    How to use? Give me a primer.
    [usage]
    
We will introduce the usage of the `w7x` package with a teaser.
Have a loo at this `introductory presentation  <https://gitlab.mpcdf.mpg.de/dboe/w7x/-/blob/master/docs/presentation/presentation.ipynb>`_.
After that you will find help in the api reference.

Given the followin frontmatter:

.. code-block:: python

    import w7x
    import tfields
    from w7x.simulation.vmec import Vmec
    from w7x.simulation.backends.nn.vmec import VmecNNBackend
    from w7x.simulation.extender import Extender
    from w7x.simulation.flt import FieldLineTracer
    from w7x.simulation.components import Components

    vmec = Vmec()
    extender = Extender()
    extender.strategy = "web_service"
    flt = FieldLineTracer()
    comps = Components()

    phi = 0

    state = w7x.State(
        w7x.config.Assemblies.Pfcs(),
        w7x.config.CoilSets.Ideal(),
        w7x.config.Plasma.Vacuum(),
    )
    params = dict(
        seeds=tfields.Points3D([[5.8, 0.0, 0.0]], coord_sys="cartesian"),
        phi=phi,
        n_points=100,
    )
    kwargs = dict(scheduler=dict(n_workers=4))

Below we demonstrate the building of a simulation pipeline (graph) and the distributed computation:
    
..
    [essence-start]

.. code-block::

    import w7x
   
    vmec.strategy = "local"  # interchangeable
   
    with w7x.distribute(True):
        graph = vmec.free_boundary(state)
        graph = extender.field(graph)
        graph = flt.trace_poincare(
           graph,
           **params
        )
        graph = w7x.State.merged(  # parallel
            graph,
            comps.mesh_slice(state, phi=phi)
        )
   
        state = w7x.compute(graph, **kwargs)
        state.save("my_state.dill")

..
    [essence-end]

You will see the distributed computation in the task graph

.. image:: ../../images/distributed_computing.gif

spawned by :class:`w7x.compute<w7x.core.compute>`.
