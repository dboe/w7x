import logging
import unittest.mock

import pytest

try:
    import matplotlib
except ModuleNotFoundError:
    matplotlib = None
try:
    import numpy as np
except ModuleNotFoundError:
    np = None


# Add auto-imports for doctests
if np is not None:

    @pytest.fixture(autouse=True)
    def add_np(doctest_namespace):
        doctest_namespace["np"] = np


# Patching plotting
@pytest.fixture(scope="session", autouse=True)
def default_session_fixture(request):
    """
    :type request: _pytest.python.SubRequest
    :return:
    """
    log = logging.getLogger()
    log.info("Patching `show`")
    patch = unittest.mock.patch("matplotlib.pyplot.show")
    if matplotlib is not None:
        if not request.config.getoption("--plot"):
            patch.start()


def pytest_addoption(parser):
    if matplotlib is not None:
        parser.addoption(
            "--plot", action="store_true", default=False, help="Activate plot tests"
        )
    parser.addoption(
        "--slow", action="store_true", default=False, help="Activate slow tests"
    )
    parser.addoption(
        "--web_service",
        action="store_true",
        default=False,
        help="Activate web_service tests",
    )



def pytest_configure(config, items):
    if matplotlib is not None:
        config.addinivalue_line(
            "markers", "plot: mark test as requiring visual assertion by human"
        )
    config.addinivalue_line("markers", "slow: mark test as slow")
    config.addinivalue_line(
        "markers", "web_service: mark test as computing on web_service"
    )

    skip_slow = pytest.mark.skip(reason="need --slow option to run")
    skip_web_service = pytest.mark.skip(reason="need --web_service option to run")
    for item in items:
        if "slow" in item.keywords and not config.getoption("--slow"):
            item.add_marker(skip_slow)
        if "web_service" in item.keywords and not config.getoption("--web_service"):
            item.add_marker(skip_web_service)
