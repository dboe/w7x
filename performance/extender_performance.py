"""
Example on how to run VMEC.

Make sure you have set the correct path in the `~/.w7x/config.cfg` file.

How to run me?

$ module load anaconda # if you are logged in the HPC
$ python examples/vmec.py
"""

import logging
import time
import numpy as np
import matplotlib.pyplot as plt

import tfields
import w7x
from w7x.simulation.vmec import Wout
from w7x.simulation.extender import Extender

logging.basicConfig(level=logging.DEBUG)

vmec_id = w7x.config.vmec.test.example_vmec_id
wout = Wout(file_id=vmec_id)
state = wout.to_state()

extender = Extender()

# You can change the resolution of the grid by passing the points explicitly:
# Here we show with a dummy grid which has uselessly small resolution
times = []
nums = [1] * 10 + [10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000]
for n_points in nums:
    tic = time.time()
    state_new = extender.field(
        state,
        points=tfields.TensorGrid.empty(
            (4.05, 6.75, n_points * 1j),
            (0, 2 * np.pi / 5, 1j),  # np.pi / 5 -> 5 field periods
            (-1.35, 1.35, 1j),
            coord_sys=tfields.bases.CYLINDER,
            iter_order=[1, 0, 2],
        ),
        n_proc=1,
    )
    toc = time.time()
    times.append(toc - tic)

plt.scatter(nums, times)
plt.show()
