from tools.process import run_shell_command, run_shell_command_with_output


def print_changelog_extract():
    r"""
    Equivalent to awk command
        @awk '/^## v[0-9]+\.[0-9]+\.[0-9]+ \([0-9]{4}-[0-9]{2}-[0-9]{2}\)/ {count++} count<=1' CHANGELOG.md
    """
    import re

    with open("CHANGELOG.md", "r") as f:
        lines = f.readlines()

    count = 0
    for line in lines:
        if re.match(
            r"^## v[0-9]+\.[0-9]+\.[0-9]+ \([0-9]{4}-[0-9]{2}-[0-9]{2}\)", line
        ):
            count += 1
            if count > 1:
                break
        print(line.strip())


def publish(part="patch", dry_run=False):
    """
    Important:
        This assumes you have made sure this is clean
    """
    dry_run = "" if not dry_run else " --dry-run"
    new_version = run_shell_command_with_output(
        f"poetry version {part} -s" + dry_run
    ).split("\n")[0]
    run_shell_command(f"git commit -am 'chore(release): v{new_version}'" + dry_run)
    run_shell_command(f"git tag v{new_version}" + dry_run)
    run_shell_command("cz ch --incremental" + dry_run)  # generate changelog
    run_shell_command("git add CHANGELOG.md" + dry_run)
    run_shell_command(
        "git commit -am 'chore(changelog): incremental update of release changelog'"
        + dry_run
    )
    run_shell_command("git push" + dry_run)
    run_shell_command("git push --tags" + dry_run)
    print("Publishing done.")
    print_changelog_extract()
    print(
        "Use the above (↑) release-note to create a release @ https://git.ipp-hgw.mpg.de/dboe/w7x_ana/-/releases"
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--part",
        type=str,
        default="patch",
        help="The part of the version to be bumped",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
    )
    args = parser.parse_args()
    part = args.part
    dry_run = args.dry_run
    publish(part=part, dry_run=dry_run)
