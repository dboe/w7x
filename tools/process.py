import logging
import shlex
import subprocess

LOGGER = logging.getLogger(__name__)


def run_shell_command(command_line, *args, **kwargs):
    command_line_args = shlex.split(command_line)
    LOGGER.debug(f"Running command '{command_line}'")
    return subprocess.check_call(command_line_args, *args, **kwargs)


def run_shell_command_with_output(command_line, *args, **kwargs):
    command_line_args = shlex.split(command_line)
    LOGGER.debug(f"Running command '{command_line}'")
    return (
        subprocess.check_output(command_line_args, *args, **kwargs)
        .decode("utf-8")
        .rstrip("\n")
    )
