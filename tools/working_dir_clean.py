import subprocess
import sys


def check_git_status():
    git_status = subprocess.check_output(["git", "status", "--porcelain"])
    if git_status:
        print("Your Git status is not clean! Clean up first.")
        print(git_status.decode("utf-8"))
        sys.exit(1)


if __name__ == "__main__":
    check_git_status()
